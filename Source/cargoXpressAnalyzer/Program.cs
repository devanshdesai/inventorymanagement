﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Services.Contract;
using CargoXpress.Services.V1;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cargoXpressAnalyzer
{
    class Program
    {
        public static readonly AbstractJobServices abstractJobServices;

        //public Program(AbstractJobServices abstractJobServices)
        //{
        //    this.abstractJobServices = abstractJobServices;
        //}

        static void Main(string[] args)
        {
            //DateTime start = Convert.ToDateTime(DateTime.Now.ToString("dd-MM-yyyy " + "08:00:00")+" AM");
            //DateTime end = Convert.ToDateTime(DateTime.Now.ToString("dd-MM-yyyy " + "05:00:00")+" PM");
            //TimeSpan timeSpan = new TimeSpan(0, 60, 0);
            //GetAllTime(start,end,60);

           

            if (Convert.ToString(ConfigurationManager.AppSettings["Type"]) == "1")
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
                DataTable dt = new DataTable();
                string todayDate = DateTime.Now.ToString("dd-MMM-yyyy");
                string var_sql = "select * from CarrierPricingPlan where ExpiryDate='" + todayDate + "'";
                SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                sda.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var_sql = "update CarrierPricingPlan set IsActive=0 where Id=" + Convert.ToInt32(dt.Rows[i]["Id"]);
                    con.Open();
                    SqlCommand cmd = new SqlCommand(var_sql, con);
                    cmd.ExecuteNonQuery();
                    con.Close();

                    if (Convert.ToInt32(dt.Rows[i]["UserType"]) == 2)
                    {
                        var_sql = "update Carriers set IsPlanActive=0 where Id=" + Convert.ToInt32(dt.Rows[i]["CarrierId"]);
                    }

                    if (Convert.ToInt32(dt.Rows[i]["UserType"]) == 3)
                    {
                        var_sql = "update Shippers set IsPlanActive=0 where Id=" + Convert.ToInt32(dt.Rows[i]["CarrierId"]);
                    }

                    con.Open();
                    cmd = new SqlCommand(var_sql, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            if (Convert.ToString(ConfigurationManager.AppSettings["Type"]) == "2")
            {
                //AbstractJobServices abstractJobServices = new JobServices();
                //dao dao = new dao(abstractJobServices);
                //dao.JobsData();

                SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
                //string var_sql = "select * from Job where JobStatusId in (1,2) and (CarrierId IS NULL OR CarrierId = 0)";
                //DataTable dtJob = new DataTable();
                //SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                //sda.Fill(dtJob);

                string var_sql = "select CC.*,C.* from Carriers CC left join Countries C on C.Id=CC.CountryId where (CC.DeviceToken is not null OR CC.DeviceToken != '')";
                DataTable dtCarriers = new DataTable();
                SqlDataAdapter sdaCarriers = new SqlDataAdapter(var_sql, con);
                sdaCarriers.Fill(dtCarriers);

                for (int i = 0; i < dtCarriers.Rows.Count; i++)
                {
                    var_sql = "select * from Job where JobStatusId in (1,2) and TruckTypeId in (select TruckTypeId from Trucks where CarrierId=" + Convert.ToInt32(dtCarriers.Rows[i]["Id"]) + ") and PickUpShipperAddressId in (select CountryId from CarrierPreferedDestinations where CarrierId=" + Convert.ToInt32(dtCarriers.Rows[i]["Id"]) + ") and DropOffShipperAddressId in (select CountryId from CarrierPreferedDestinations where CarrierId=" + Convert.ToInt32(dtCarriers.Rows[i]["Id"]) + ")";
                    DataTable dtJob = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                    sda.Fill(dtJob);

                    for(int j=0; j < dtJob.Rows.Count; j++)
                    {
                        string text = "NEW JOB (" + Convert.ToString(dtJob.Rows[j]["JobNo"]) + ") IS AVAILABLE FOR YOU";
                        var_sql = "select count(*) from Notification where UserId=" + Convert.ToInt32(dtCarriers.Rows[i]["Id"]) + " and UserType=2 and Text='"+ text + "'";
                        SqlCommand cmd = new SqlCommand(var_sql, con);
                        con.Open();
                        int count = (int)cmd.ExecuteScalar();
                        con.Close();

                        if(count == 0)
                        {
                            string title = "Aurora - New Job Available";
                            string mobile = Convert.ToString(dtCarriers.Rows[i]["CountryCode"]) + "" + Convert.ToString(dtCarriers.Rows[i]["PhoneNumber"]);
                            EmailHelper.SendPushNotification(Convert.ToString(dtCarriers.Rows[i]["DeviceToken"]), text, title, mobile);

                            var_sql = "insert into Notification (UserId,UserType,Text,CreatedDate,IsRead) values (" + Convert.ToInt32(dtCarriers.Rows[i]["Id"]) + ",2,'"+text+"',getdate(),0)";
                            cmd = new SqlCommand(var_sql, con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }

            }

            if (Convert.ToString(ConfigurationManager.AppSettings["Type"]) == "3")
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
                
                string var_sql = "select * from Job where JobStatusId in (4,5,6,7,8,9,19)";
                DataTable dtJobs = new DataTable();
                SqlDataAdapter sdaJobs = new SqlDataAdapter(var_sql, con);
                sdaJobs.Fill(dtJobs);

                for(int i=0; i<dtJobs.Rows.Count; i++)
                {
                    var_sql = "select SH.*,C.* from Shippers SH left join Countries C on C.Id=SH.CountryId where SH.Id=" + Convert.ToInt32(dtJobs.Rows[i]["ShipperId"]);
                    DataTable dtShipper = new DataTable();
                    SqlDataAdapter sdaShipper = new SqlDataAdapter(var_sql, con);
                    sdaShipper.Fill(dtShipper);

                    var_sql = "select SH.*,C.* from Carriers SH left join Countries C on C.Id=SH.CountryId where SH.Id=" + Convert.ToInt32(dtJobs.Rows[i]["CarrierId"]);
                    DataTable dtCarriers = new DataTable();
                    SqlDataAdapter sdaCarriers = new SqlDataAdapter(var_sql, con);
                    sdaCarriers.Fill(dtCarriers);

                    var_sql = "select * from JobTruckDrivers where JobId="+Convert.ToInt32(dtJobs.Rows[i]["Id"]);
                    DataTable dtJobTruckDrivers = new DataTable();
                    SqlDataAdapter sdaJobTruckDrivers = new SqlDataAdapter(var_sql, con);
                    sdaJobTruckDrivers.Fill(dtJobTruckDrivers);

                    for(int j=0; j<dtJobTruckDrivers.Rows.Count; j++)
                    {
                        
                        var_sql = "select Top 1 * from RouteMaster where JobTruckDriverId=" + Convert.ToInt32(dtJobTruckDrivers.Rows[j]["Id"]);
                        DataTable dtRouteMaster = new DataTable();
                        SqlDataAdapter sdaRouteMaster = new SqlDataAdapter(var_sql, con);
                        sdaRouteMaster.Fill(dtRouteMaster);
                        
                        if(dtRouteMaster.Rows.Count > 0)
                        {
                            double distance = 0;
                            var_sql = "select Top 1 * from JobDriverLocation where DriverId=" + Convert.ToInt32(dtJobTruckDrivers.Rows[j]["DriverId"])+" and JobId="+ Convert.ToInt32(dtJobs.Rows[i]["Id"])+" order by id desc";
                            DataTable dtJobDriverLocation = new DataTable();
                            SqlDataAdapter sdaJobDriverLocation = new SqlDataAdapter(var_sql, con);
                            sdaJobDriverLocation.Fill(dtJobDriverLocation);

                            if(dtJobDriverLocation.Rows.Count > 0)
                            {
                                var sCoord = new GeoCoordinate(Convert.ToDouble(dtRouteMaster.Rows[0]["PickUpLat"]), Convert.ToDouble(dtRouteMaster.Rows[0]["PickUpLong"]));
                                var eCoord = new GeoCoordinate(Convert.ToDouble(dtJobDriverLocation.Rows[0]["Latitude"]), Convert.ToDouble(dtJobDriverLocation.Rows[0]["Longitude"]));
                                distance = Convert.ToDouble((sCoord.GetDistanceTo(eCoord) / 1000).ToString("00.00"));

                                string title = "Aurora - Daily Update";
                                string body = "The truck/driver has covered a distance of " + distance + " km for Job ("+ConvertTo.String(dtJobs.Rows[i]["JobNo"]) +")";

                                EmailHelper.SendPushNotification(ConvertTo.String(dtShipper.Rows[0]["DeviceToken"]), body, title, Convert.ToString(dtShipper.Rows[0]["CountryCode"]) + "" + Convert.ToString(dtShipper.Rows[0]["PhoneNumber"]));

                                EmailHelper.SendPushNotification(ConvertTo.String(dtCarriers.Rows[0]["DeviceToken"]), body, title, Convert.ToString(dtCarriers.Rows[0]["CountryCode"]) + "" + Convert.ToString(dtCarriers.Rows[0]["PhoneNumber"]));

                            }
                        }

                    }
                }
            }
        }

        public static List<string> GetAllTime(DateTime start, DateTime end, int interval)
        {
            List<string> times = new List<string>();
            DateTime currentStart = start;
            times.Add(currentStart.ToString("hh:mm tt"));
            while (currentStart < end)
            {
                //Console.WriteLine(String.Format("{0} - {1}", currentStart, currentStart.Add(work)));
                times.Add(currentStart.AddMinutes(interval).ToString("hh:mm tt"));
                currentStart = Convert.ToDateTime(currentStart.AddMinutes(interval));
                //currentStart = ;
            }
            return times;
        }

        //public  void JobsData()
        //{
        //    PageParam pageParam = new PageParam();
        //    var quote = abstractJobServices.Job_All(pageParam, "", "", 0, 0, 0, 0, 0, 0, null, null, null, "NewJob", 0, 0, false, "", 0);

        //}
    }

    public class dao
    {
        public readonly AbstractJobServices abstractJobServices;
        public dao(AbstractJobServices abstractJobServices)
        {
            this.abstractJobServices = abstractJobServices;
        }

        public void JobsData()
        {
            PageParam pageParam = new PageParam();
            var quote = abstractJobServices.Job_All(pageParam, "", "", 0, 0, 0, 0, 0, 0, null, null, null, "NewJob", 0, 0, false, "", 0);

        }
    }
}
