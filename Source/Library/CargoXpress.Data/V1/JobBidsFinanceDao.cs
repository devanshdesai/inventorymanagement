﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Data.V1
{
    public class JobBidsFinanceDao : AbstractJobBidsFinanceDao
    {

        public override PagedList<AbstractJobBidsFinance> JobBidsFinance_All(PageParam pageParam, string search,int JobBidId)
        {
            PagedList<AbstractJobBidsFinance> JobBidsFinance = new PagedList<AbstractJobBidsFinance>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@JobBidId", JobBidId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBidsFinance_All, param, commandType: CommandType.StoredProcedure);
                JobBidsFinance.Values.AddRange(task.Read<JobBidsFinance>());
                JobBidsFinance.TotalRecords=task.Read<long>().SingleOrDefault();
            }
            return JobBidsFinance;
        }

        

        public override SuccessResult<AbstractJobBidsFinance> JobBidsFinance_ById(int Id)
        {
            SuccessResult<AbstractJobBidsFinance> JobBidsFinance = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBidsFinance_ById, param, commandType: CommandType.StoredProcedure);
                JobBidsFinance=task.Read<SuccessResult<AbstractJobBidsFinance>>().SingleOrDefault();
                JobBidsFinance.Item=task.Read<JobBidsFinance>().SingleOrDefault();
            }

            return JobBidsFinance;
        }

        public override bool JobBidsFinance_Delete(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.JobBidsFinance_Delete, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }
            return result;
        }

       

        public override SuccessResult<AbstractJobBidsFinance> JobBidsFinance_Upsert(AbstractJobBidsFinance abstractJobBidsFinance)
        {
            SuccessResult<AbstractJobBidsFinance> JobBidsFinance = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobBidsFinance.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobBidId", abstractJobBidsFinance.JobBidId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@FinanceType", abstractJobBidsFinance.FinanceType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Percentage", abstractJobBidsFinance.Percentage, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Days", abstractJobBidsFinance.Days, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBidsFinance_Upsert, param, commandType: CommandType.StoredProcedure);
                JobBidsFinance=task.Read<SuccessResult<AbstractJobBidsFinance>>().SingleOrDefault();
                JobBidsFinance.Item=task.Read<JobBidsFinance>().SingleOrDefault();
            }

            return JobBidsFinance;
        }
    }
}
