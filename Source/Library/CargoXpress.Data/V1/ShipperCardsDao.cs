﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class ShipperCardsDao : AbstractShipperCardsDao
    {
        public override SuccessResult<AbstractShipperCards> ShipperCards_Upsert(AbstractShipperCards abstractShipperCards)
        {
            SuccessResult<AbstractShipperCards> ShipperCards = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShipperCards.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId", abstractShipperCards.ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CardNumber", abstractShipperCards.CardNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ExpiryDate", abstractShipperCards.ExpiryDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Cvv", abstractShipperCards.Cvv, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShipperCards_Upsert, param, commandType: CommandType.StoredProcedure);
                ShipperCards = task.Read<SuccessResult<AbstractShipperCards>>().SingleOrDefault();
                ShipperCards.Item = task.Read<ShipperCards>().SingleOrDefault();
            }

            return ShipperCards;
        }

        public override SuccessResult<AbstractShipperCards> ShipperCards_ById(int Id)
        {
            SuccessResult<AbstractShipperCards> ShipperCards = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShipperCards_ById, param, commandType: CommandType.StoredProcedure);
                ShipperCards = task.Read<SuccessResult<AbstractShipperCards>>().SingleOrDefault();
                ShipperCards.Item = task.Read<ShipperCards>().SingleOrDefault();
            }

            return ShipperCards;
        }

        public override PagedList<AbstractShipperCards> ShipperCards_ByShipperId(PageParam pageParam, string search,int ShipperId)
        {
            PagedList<AbstractShipperCards> ShipperCards = new PagedList<AbstractShipperCards>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShipperId", ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShipperCards_ByShipperId, param, commandType: CommandType.StoredProcedure);
                ShipperCards.Values.AddRange(task.Read<ShipperCards>());
                ShipperCards.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShipperCards;
            }

            public override SuccessResult<AbstractShipperCards> ShipperCards_Delete(AbstractShipperCards abstractShipperCards)
            {
            SuccessResult<AbstractShipperCards> ShipperCards = null;        
            var param = new DynamicParameters();

            param.Add("@Id", abstractShipperCards.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId", abstractShipperCards.ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShipperCards_Delete, param, commandType: CommandType.StoredProcedure);
                ShipperCards = task.Read<SuccessResult<AbstractShipperCards>>().SingleOrDefault();
                ShipperCards.Item = task.Read<ShipperCards>().SingleOrDefault();
            }
            return ShipperCards;
            }

        public override SuccessResult<AbstractShipperCards> ShipperCards_IsDefault(int Id)
           {
            SuccessResult<AbstractShipperCards> ShipperCards = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShipperCards_IsDefault, param, commandType: CommandType.StoredProcedure);
                ShipperCards = task.Read<SuccessResult<AbstractShipperCards>>().SingleOrDefault();
                ShipperCards.Item = task.Read<ShipperCards>().SingleOrDefault();
            }

            return ShipperCards;
            }
        

    }

    }
