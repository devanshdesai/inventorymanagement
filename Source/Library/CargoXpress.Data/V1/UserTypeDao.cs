﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class UserTypeDao : AbstractUserTypeDao
    {
       

        public override PagedList<AbstractUserType> UserType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractUserType> UserType = new PagedList<AbstractUserType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserType_All, param, commandType: CommandType.StoredProcedure);
                UserType.Values.AddRange(task.Read<UserType>());
                UserType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserType;
            }

            

    }

    }
