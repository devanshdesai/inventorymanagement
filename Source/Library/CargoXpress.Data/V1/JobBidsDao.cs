﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobBidsDao : AbstractJobBidsDao
    {
        public override SuccessResult<AbstractJobBids> JobBids_Upsert(AbstractJobBids abstractJobBids)
        {
            SuccessResult<AbstractJobBids> JobBids = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobBids.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobBids.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractJobBids.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Price", abstractJobBids.Price, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@JobCompletionDays", abstractJobBids.JobCompletionDays, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Description", abstractJobBids.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentType", abstractJobBids.PaymentType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsFinance", abstractJobBids.IsFinance, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@Percentage", abstractJobBids.Percentage, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@BreakDown", abstractJobBids.BreakDown, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBids_Upsert, param, commandType: CommandType.StoredProcedure);
                JobBids = task.Read<SuccessResult<AbstractJobBids>>().SingleOrDefault();
                JobBids.Item = task.Read<JobBids>().SingleOrDefault();
            }

            return JobBids;
        }

        public override SuccessResult<AbstractJobBids> JobBids_ById(int Id)
        {
            SuccessResult<AbstractJobBids> JobBids = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBids_ById, param, commandType: CommandType.StoredProcedure);
                JobBids = task.Read<SuccessResult<AbstractJobBids>>().SingleOrDefault();
                JobBids.Item = task.Read<JobBids>().SingleOrDefault();
            }

            return JobBids;
        }

        public override PagedList<AbstractJobBids> JobBids_ByJobId(PageParam pageParam, int JobId, int CarrierId)
        {


            PagedList<AbstractJobBids> JobBids = new PagedList<AbstractJobBids>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBids_ByJobId, param, commandType: CommandType.StoredProcedure);
                JobBids.Values.AddRange(task.Read<JobBids>());
                JobBids.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobBids;
        }

        public override SuccessResult<AbstractJobBids> JobBidStatus_Update(AbstractJobBids abstractJobBids)
        {
            SuccessResult<AbstractJobBids> JobBids = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobBids.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobBidStatus", abstractJobBids.JobBidStatus, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractJobBids.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBidStatus_Update, param, commandType: CommandType.StoredProcedure);
                JobBids = task.Read<SuccessResult<AbstractJobBids>>().SingleOrDefault();
                JobBids.Item = task.Read<JobBids>().SingleOrDefault();
            }

            return JobBids;
        }

    }

}
