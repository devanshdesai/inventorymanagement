﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CarriersDao : AbstractCarriersDao
    {
        public override SuccessResult<AbstractCarriers> Carriers_Upsert(AbstractCarriers abstractCarriers)
        {
            SuccessResult<AbstractCarriers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCarriers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Email", abstractCarriers.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractCarriers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractCarriers.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractCarriers.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyName", abstractCarriers.CompanyName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TradingLicense", abstractCarriers.TradingLicense, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Tin", abstractCarriers.Tin, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RegistrantId", abstractCarriers.RegistrantId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractCarriers.CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", abstractCarriers.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FactoringService", abstractCarriers.FactoringService, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@TruckTypeId", abstractCarriers.TruckTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCarriers.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCarriers.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", abstractCarriers.DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_Upsert, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractCarriers>>().SingleOrDefault();
                Carriers.Item = task.Read<Carriers>().SingleOrDefault();
            }

            return Carriers;
        }

        public override SuccessResult<AbstractCarriers> Carriers_ProfileUpdate(AbstractCarriers abstractCarriers)
        {
            SuccessResult<AbstractCarriers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCarriers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@FileURL", abstractCarriers.FileURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCarriers.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_ProfileUpdate, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractCarriers>>().SingleOrDefault();
                Carriers.Item = task.Read<Carriers>().SingleOrDefault();
            }

            return Carriers;
        }

        public override SuccessResult<AbstractCarriers> Carriers_ById(int Id)
        {
            SuccessResult<AbstractCarriers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_ById, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractCarriers>>().SingleOrDefault();
                Carriers.Item = task.Read<Carriers>().SingleOrDefault();
            }

            return Carriers;
        }

        public override PagedList<AbstractCarriers> Carriers_All(PageParam pageParam, string search,string Name,string Email,int CountryId,string PhoneNumber, bool? IsActive)
        {
            PagedList<AbstractCarriers> Carriers = new PagedList<AbstractCarriers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Name", Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", IsActive != null ? IsActive.Value : IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_All, param, commandType: CommandType.StoredProcedure);
                Carriers.Values.AddRange(task.Read<Carriers>());
                Carriers.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Carriers;
            }

            public override SuccessResult<AbstractCarriers> Carriers_Login(AbstractCarriers abstractCarriers)
            {
            SuccessResult<AbstractCarriers> Carriers = null;        
            var param = new DynamicParameters();
            param.Add("@CountryId", abstractCarriers.CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", abstractCarriers.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractCarriers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LoginType", abstractCarriers.LoginType, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", abstractCarriers.DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_Login, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractCarriers>>().SingleOrDefault();
                Carriers.Item = task.Read<Carriers>().SingleOrDefault();
            }
            return Carriers;
            }

        public override SuccessResult<AbstractCarriers> Carriers_ActInact(int Id)
           {
            SuccessResult<AbstractCarriers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_ActInact, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractCarriers>>().SingleOrDefault();
                Carriers.Item = task.Read<Carriers>().SingleOrDefault();
            }

            return Carriers;
            }
        public override SuccessResult<AbstractCarriers> Carriers_Approve(int Id)
        {
            SuccessResult<AbstractCarriers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_Approve, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractCarriers>>().SingleOrDefault();
                Carriers.Item = task.Read<Carriers>().SingleOrDefault();
            }
            return Carriers;
        }

        public override SuccessResult<AbstractCarriers> Carriers_ChangePassword(AbstractCarriers abstractCarriers)
        {
            SuccessResult<AbstractCarriers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCarriers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OldPassword", abstractCarriers.OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", abstractCarriers.NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", abstractCarriers.ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", abstractCarriers.Type, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_ChangePassword, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractCarriers>>().SingleOrDefault();
                Carriers.Item = task.Read<Carriers>().SingleOrDefault();
            }

            return Carriers;
        }

        public override SuccessResult<AbstractCarriers> Carriers_EmailVerify(int Id)
        {
            SuccessResult<AbstractCarriers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_EmailVerify, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractCarriers>>().SingleOrDefault();
                Carriers.Item = task.Read<Carriers>().SingleOrDefault();
            }
            return Carriers;
        }

        public override SuccessResult<AbstractCarriers> Carriers_MobileVerify(int Id)
        {
            SuccessResult<AbstractCarriers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Carriers_MobileVerify, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractCarriers>>().SingleOrDefault();
                Carriers.Item = task.Read<Carriers>().SingleOrDefault();
            }
            return Carriers;
        }

        public override bool Carriers_Logout(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Carriers_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        public override bool Carriers_ResetPassword(int Id,string Password)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Password", Password, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Carriers_ResetPassword, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

    }

    }
