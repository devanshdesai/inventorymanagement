﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class MasterJobStatusDao : AbstractMasterJobStatusDao
    {
           
        public override PagedList<AbstractMasterJobStatus> MasterJobStatus_All(PageParam pageParam, String search, int JobStatusTypeId)
        {
            

            PagedList<AbstractMasterJobStatus> MasterJobStatus = new PagedList<AbstractMasterJobStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@JobStatusTypeId", JobStatusTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterJobStatus_All, param, commandType: CommandType.StoredProcedure);
                MasterJobStatus.Values.AddRange(task.Read<MasterJobStatus>());
                MasterJobStatus.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterJobStatus;
        }

    }

    }
