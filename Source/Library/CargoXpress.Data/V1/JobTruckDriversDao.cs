﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobTruckDriversDao : AbstractJobTruckDriversDao
    {
        public override SuccessResult<AbstractJobTruckDrivers> JobTruckDrivers_Upsert(AbstractJobTruckDrivers abstractJobTruckDrivers)
        {
            SuccessResult<AbstractJobTruckDrivers> JobTruckDrivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobTruckDrivers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobTruckDrivers.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", abstractJobTruckDrivers.TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", abstractJobTruckDrivers.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobTruckDrivers_Upsert, param, commandType: CommandType.StoredProcedure);
                JobTruckDrivers = task.Read<SuccessResult<AbstractJobTruckDrivers>>().SingleOrDefault();
                JobTruckDrivers.Item = task.Read<JobTruckDrivers>().SingleOrDefault();
            }

            return JobTruckDrivers;
        }

        

        public override SuccessResult<AbstractJobTruckDrivers> JobTruckDrivers_ById(int Id)
        {
            SuccessResult<AbstractJobTruckDrivers> JobTruckDrivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobTruckDrivers_ById, param, commandType: CommandType.StoredProcedure);
                JobTruckDrivers = task.Read<SuccessResult<AbstractJobTruckDrivers>>().SingleOrDefault();
                JobTruckDrivers.Item = task.Read<JobTruckDrivers>().SingleOrDefault();
            }

            return JobTruckDrivers;
        }

        public override PagedList<AbstractJobTruckDrivers> JobTruckDrivers_ByJobId(PageParam pageParam, string search,int JobId, int DriverId=0,int TruckId=0)
        {
            PagedList<AbstractJobTruckDrivers> JobTruckDrivers = new PagedList<AbstractJobTruckDrivers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobTruckDrivers_ByJobId, param, commandType: CommandType.StoredProcedure);
                JobTruckDrivers.Values.AddRange(task.Read<JobTruckDrivers>());
                JobTruckDrivers.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobTruckDrivers;
            }


        public override bool JobTruckDrivers_Delete(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.JobTruckDrivers_Delete, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        

    }

    }
