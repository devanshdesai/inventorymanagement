﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class MasterDriverCommentsDao : AbstractMasterDriverCommentsDao
    {
           
        public override PagedList<AbstractMasterDriverComments> MasterDriverComments_All(PageParam pageParam)
        {
            

            PagedList<AbstractMasterDriverComments> MasterDriverComments = new PagedList<AbstractMasterDriverComments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterDriverComments_All, param, commandType: CommandType.StoredProcedure);
                MasterDriverComments.Values.AddRange(task.Read<MasterDriverComments>());
                MasterDriverComments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterDriverComments;
        }

    }

    }
