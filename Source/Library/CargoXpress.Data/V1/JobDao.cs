﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobDao : AbstractJobDao
    {
        public override SuccessResult<AbstractJob> Job_Upsert(AbstractJob abstractJob)
        {
            SuccessResult<AbstractJob> Job = null;
            var param = new DynamicParameters();

            //param.Add("@Id", abstractJob.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@ShipperId", abstractJob.ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@TruckTypeId", abstractJob.TruckTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@TruckServiceId", abstractJob.TruckServiceId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@SubTruckServiceId", abstractJob.SubTruckServiceId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@NoOfContainer", abstractJob.NoOfContainer, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@ActualTonage", abstractJob.ActualTonage, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            //param.Add("@JobTonage", abstractJob.JobTonage, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            //param.Add("@TotalPallets", abstractJob.TotalPallets, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            //param.Add("@ActualWeight", abstractJob.ActualWeight, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            //param.Add("@JobWeight", abstractJob.JobWeight, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            //param.Add("@Cbm", abstractJob.Cbm, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            //param.Add("@CommodityType", abstractJob.CommodityType, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@HsCode", abstractJob.HsCode, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@PickUpShipperAddressId", abstractJob.PickUpShipperAddressId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@DropOffShipperAddressId", abstractJob.DropOffShipperAddressId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@SpecialMessage", abstractJob.SpecialMessage, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@ShipperTentativeEndDate", abstractJob.ShipperTentativeEndDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@Id", abstractJob.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId", abstractJob.ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckTypeId", abstractJob.TruckTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckServiceId", abstractJob.TruckServiceId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@NoOfContainer", abstractJob.NoOfContainer, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ActualTonage", abstractJob.ActualTonage, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@TotalPallets", abstractJob.TotalPallets, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Cbm", abstractJob.Cbm, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CommodityType", abstractJob.CommodityType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@HsCode", abstractJob.HsCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SpecialMessage", abstractJob.SpecialMessage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PickUpAddress", abstractJob.PickUpAddress, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PickUpAddressLongitude", abstractJob.PickUpAddressLongitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PickUpAddressLatitude", abstractJob.PickUpAddressLatitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DropOffAddress", abstractJob.DropOffAddress, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DropOffAddressLongitude", abstractJob.DropOffAddressLongitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DropOffAddressLatitude", abstractJob.DropOffAddressLatitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsFargile", abstractJob.IsFargile, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsExpres", abstractJob.IsExpres, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@ExpressDate", abstractJob.ExpressDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PickupDate", abstractJob.PickupDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Distance", abstractJob.Distance, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@PickUpShipperAddressId", abstractJob.PickUpShipperAddressId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DropOffShipperAddressId", abstractJob.DropOffShipperAddressId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckLength", abstractJob.TruckLength, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ParentId", abstractJob.ParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractJob.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", abstractJob.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", abstractJob.TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CubicMeter", abstractJob.CubicMeter, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Liters", abstractJob.Liters, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Cars", abstractJob.Cars, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeliveryItem", abstractJob.DeliveryItem, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsCrossBorder", abstractJob.IsCrossBorder, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@Kg", abstractJob.Kg, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Currency", abstractJob.Currency, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Job_Upsert, param, commandType: CommandType.StoredProcedure);
                Job = task.Read<SuccessResult<AbstractJob>>().SingleOrDefault();
                Job.Item = task.Read<Job>().SingleOrDefault();
            }

            return Job;
        }

        public override SuccessResult<AbstractJob> Job_ById(int Id)
        {
            SuccessResult<AbstractJob> Job = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Job_ById, param, commandType: CommandType.StoredProcedure);
                Job = task.Read<SuccessResult<AbstractJob>>().SingleOrDefault();
                Job.Item = task.Read<Job>().SingleOrDefault();
            }

            return Job;
        }

        public override PagedList<AbstractJob> Job_All(PageParam pageParam, string Search, string JobNo, int ShipperId, int CarrierId, int DriverId, int TruckId, int JobStatusId, int TruckTypeId, string JobStatus, int PickUpShipperAddressId, int DropOffShipperAddressId, bool IsJobWithIssues, string PickupDate, int TruckServiceId, DateTime? CarrierJobStartDate, DateTime? CarrierJobEndDate)
        {


            PagedList<AbstractJob> Job = new PagedList<AbstractJob>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@JobNo", JobNo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShipperId", ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobStatusId", JobStatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckTypeId", TruckTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input); param.Add("@PickUpShipperAddressId", PickUpShipperAddressId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DropOffShipperAddressId", DropOffShipperAddressId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsJobWithIssues", IsJobWithIssues, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@PickupDate", PickupDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TruckServiceId", TruckServiceId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierJobStartDate", CarrierJobStartDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@CarrierJobEndDate", CarrierJobEndDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@JobStatus", JobStatus, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Job_All, param, commandType: CommandType.StoredProcedure);
                Job.Values.AddRange(task.Read<Job>());
                Job.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Job;
        }

        public override SuccessResult<AbstractJob> Job_AssignDriver(AbstractJob abstractJob)
        {
            SuccessResult<AbstractJob> Job = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJob.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", abstractJob.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Job_AssignDriver, param, commandType: CommandType.StoredProcedure);
                Job = task.Read<SuccessResult<AbstractJob>>().SingleOrDefault();
                Job.Item = task.Read<Job>().SingleOrDefault();
            }

            return Job;
        }

        public override SuccessResult<AbstractJob> Job_AssignTruck(AbstractJob abstractJob)
        {
            SuccessResult<AbstractJob> Job = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJob.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", abstractJob.TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Job_AssignTruck, param, commandType: CommandType.StoredProcedure);
                Job = task.Read<SuccessResult<AbstractJob>>().SingleOrDefault();
                Job.Item = task.Read<Job>().SingleOrDefault();
            }

            return Job;
        }

        public override SuccessResult<AbstractJob> Job_CarrierJobEndDate(AbstractJob abstractJob)
        {
            SuccessResult<AbstractJob> Job = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJob.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Job_CarrierJobEndDate, param, commandType: CommandType.StoredProcedure);
                Job = task.Read<SuccessResult<AbstractJob>>().SingleOrDefault();
                Job.Item = task.Read<Job>().SingleOrDefault();
            }

            return Job;
        }

        public override SuccessResult<AbstractJob> Job_CarrierJobStartDate(AbstractJob abstractJob)
        {
            SuccessResult<AbstractJob> Job = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJob.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Job_CarrierJobStartDate, param, commandType: CommandType.StoredProcedure);
                Job = task.Read<SuccessResult<AbstractJob>>().SingleOrDefault();
                Job.Item = task.Read<Job>().SingleOrDefault();
            }

            return Job;
        }

        public override SuccessResult<AbstractJob> Job_JobStatus(AbstractJob abstractJob)
        {
            SuccessResult<AbstractJob> Job = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJob.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobStatusId", abstractJob.JobStatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Job_JobStatus, param, commandType: CommandType.StoredProcedure);
                Job = task.Read<SuccessResult<AbstractJob>>().SingleOrDefault();
                Job.Item = task.Read<Job>().SingleOrDefault();
            }

            return Job;
        }

        public override SuccessResult<AbstractJob> Job_AcceptBid(AbstractJob abstractJob)
        {
            SuccessResult<AbstractJob> Job = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJob.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobBidId", abstractJob.JobBidId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Job_AcceptBid, param, commandType: CommandType.StoredProcedure);
                Job = task.Read<SuccessResult<AbstractJob>>().SingleOrDefault();
                Job.Item = task.Read<Job>().SingleOrDefault();
            }

            return Job;
        }
    }

}
