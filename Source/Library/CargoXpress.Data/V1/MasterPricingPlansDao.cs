﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class MasterPricingPlansDao : AbstractMasterPricingPlansDao
    {
        public override SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans_Upsert(AbstractMasterPricingPlans abstractMasterPricingPlans)
        {
            SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMasterPricingPlans.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PlanName", abstractMasterPricingPlans.PlanName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", abstractMasterPricingPlans.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Price", abstractMasterPricingPlans.Price, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@TimeLimit", abstractMasterPricingPlans.TimeLimit, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@AdvPayPerc", abstractMasterPricingPlans.AdvPayPerc, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@TotalNoOfInterServices", abstractMasterPricingPlans.TotalNoOfInterServices, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AdditionalServicePrice", abstractMasterPricingPlans.AdditionalServicePrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@UserType", abstractMasterPricingPlans.UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractMasterPricingPlans.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractMasterPricingPlans.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPricingPlans_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterPricingPlans = task.Read<SuccessResult<AbstractMasterPricingPlans>>().SingleOrDefault();
                MasterPricingPlans.Item = task.Read<MasterPricingPlans>().SingleOrDefault();
            }

            return MasterPricingPlans;
        }

        

        public override SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans_ById(int Id)
        {
            SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPricingPlans_ById, param, commandType: CommandType.StoredProcedure);
                MasterPricingPlans = task.Read<SuccessResult<AbstractMasterPricingPlans>>().SingleOrDefault();
                MasterPricingPlans.Item = task.Read<MasterPricingPlans>().SingleOrDefault();
            }

            return MasterPricingPlans;
        }

        public override PagedList<AbstractMasterPricingPlans> MasterPricingPlans_All(PageParam pageParam, string search="",int UserType=0)
        {
            PagedList<AbstractMasterPricingPlans> MasterPricingPlans = new PagedList<AbstractMasterPricingPlans>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserType", UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPricingPlans_All, param, commandType: CommandType.StoredProcedure);
                MasterPricingPlans.Values.AddRange(task.Read<MasterPricingPlans>());
                MasterPricingPlans.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterPricingPlans;
            }

            

        public override SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans_ActInact(int Id)
           {
            SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPricingPlans_ActInact, param, commandType: CommandType.StoredProcedure);
                MasterPricingPlans = task.Read<SuccessResult<AbstractMasterPricingPlans>>().SingleOrDefault();
                MasterPricingPlans.Item = task.Read<MasterPricingPlans>().SingleOrDefault();
            }

            return MasterPricingPlans;
            }
        

    }

    }
