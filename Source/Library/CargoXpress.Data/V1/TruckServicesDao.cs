﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class TruckServicesDao : AbstractTruckServicesDao
    {

        public override SuccessResult<AbstractTruckServices> TruckServices_Upsert(AbstractTruckServices abstractTruckServices)
        {
            SuccessResult<AbstractTruckServices> TruckServices = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTruckServices.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractTruckServices.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ParentId", abstractTruckServices.ParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckServices_Upsert, param, commandType: CommandType.StoredProcedure);
                TruckServices = task.Read<SuccessResult<AbstractTruckServices>>().SingleOrDefault();
                TruckServices.Item = task.Read<TruckServices>().SingleOrDefault();
            }

            return TruckServices;
        }

        public override SuccessResult<AbstractTruckServices> TruckServices_ById(int Id)
        {
            SuccessResult<AbstractTruckServices> TruckServices = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckServices_ById, param, commandType: CommandType.StoredProcedure);
                TruckServices = task.Read<SuccessResult<AbstractTruckServices>>().SingleOrDefault();
                TruckServices.Item = task.Read<TruckServices>().SingleOrDefault();
            }

            return TruckServices;
        }

        public override PagedList<AbstractTruckServices> TruckServices_All(PageParam pageParam, String Search, int ParentId)
        {


            PagedList<AbstractTruckServices> TruckServices = new PagedList<AbstractTruckServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ParentId", ParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckServices_All, param, commandType: CommandType.StoredProcedure);
                TruckServices.Values.AddRange(task.Read<TruckServices>());
                TruckServices.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TruckServices;
        }



        public override PagedList<AbstractTruckServices> TruckServices_ByParentId(PageParam pageParam, String Search)
        {


            PagedList<AbstractTruckServices> TruckServices = new PagedList<AbstractTruckServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckServices_ByParentId, param, commandType: CommandType.StoredProcedure);
                TruckServices.Values.AddRange(task.Read<TruckServices>());
                TruckServices.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TruckServices;
        }


    }

}
