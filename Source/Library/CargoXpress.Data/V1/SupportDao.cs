﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class SupportDao : AbstractSupportDao
    {
        public override SuccessResult<AbstractSupport> Support_Upsert(AbstractSupport abstractSupport)
        {
            SuccessResult<AbstractSupport> Support = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractSupport.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractSupport.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId ", abstractSupport.ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractSupport.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StatusId", abstractSupport.StatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Issue", abstractSupport.Issue, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserTypeId", abstractSupport.UserTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Support_Upsert, param, commandType: CommandType.StoredProcedure);
                Support = task.Read<SuccessResult<AbstractSupport>>().SingleOrDefault();
                Support.Item = task.Read<Support>().SingleOrDefault();
            }

            return Support;
        }

        public override SuccessResult<AbstractSupport> Support_Close(int Id)
        {
            SuccessResult<AbstractSupport> Support = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Support_Close, param, commandType: CommandType.StoredProcedure);
                Support = task.Read<SuccessResult<AbstractSupport>>().SingleOrDefault();
                Support.Item = task.Read<Support>().SingleOrDefault();
            }

            return Support;
        }

        public override SuccessResult<AbstractSupport> Support_ActInact(int Id)
        {
            SuccessResult<AbstractSupport> Support = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Support_ActInact, param, commandType: CommandType.StoredProcedure);
                Support = task.Read<SuccessResult<AbstractSupport>>().SingleOrDefault();
                Support.Item = task.Read<Support>().SingleOrDefault();
            }

            return Support;
        }

        public override PagedList<AbstractSupport> Support_All(PageParam pageParam, int CarrierId, int ShipperId, int JobId, int StatusId)
        {
            PagedList<AbstractSupport> Support = new PagedList<AbstractSupport>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId", ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Support_All, param, commandType: CommandType.StoredProcedure);
                Support.Values.AddRange(task.Read<Support>());
                Support.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Support;
            }



        public override SuccessResult<AbstractSupport> Support_ById(int Id)
        {
            SuccessResult<AbstractSupport> Support = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Support_ById, param, commandType: CommandType.StoredProcedure);
                Support = task.Read<SuccessResult<AbstractSupport>>().SingleOrDefault();
                Support.Item = task.Read<Support>().SingleOrDefault();
            }

            return Support;
        }

    }
}
