﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobRatingsDao : AbstractJobRatingsDao
    {

        public override SuccessResult<AbstractJobRatings> JobRatings_Upsert(AbstractJobRatings abstractJobRatings)
        {
            SuccessResult<AbstractJobRatings> JobRatings = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobRatings.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobRatings.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@RatingToShipper", abstractJobRatings.RatingToShipper, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CommentToShipper", abstractJobRatings.CommentToShipper, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", abstractJobRatings.UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobRatings_Upsert, param, commandType: CommandType.StoredProcedure);
                JobRatings = task.Read<SuccessResult<AbstractJobRatings>>().SingleOrDefault();
                JobRatings.Item = task.Read<JobRatings>().SingleOrDefault();
            }

            return JobRatings;
        }

        public override SuccessResult<AbstractJobRatings> JobRatings_ById(int Id)
        {
            SuccessResult<AbstractJobRatings> JobRatings = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobRatings_ById, param, commandType: CommandType.StoredProcedure);
                JobRatings = task.Read<SuccessResult<AbstractJobRatings>>().SingleOrDefault();
                JobRatings.Item = task.Read<JobRatings>().SingleOrDefault();
            }

            return JobRatings;
        }

        public override PagedList<AbstractJobRatings> JobRatings_ByJobId(PageParam pageParam, int JobId, int ShipperId, int CarrierId,int UserType)
        {
            

            PagedList<AbstractJobRatings> JobRatings = new PagedList<AbstractJobRatings>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId", ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserType", UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);

            //

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobRatings_ByJobId, param, commandType: CommandType.StoredProcedure);
                JobRatings.Values.AddRange(task.Read<JobRatings>());
                JobRatings.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobRatings;
        }

    }

    }
