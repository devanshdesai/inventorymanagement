﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractNotificationDao
    {
        public abstract SuccessResult<AbstractNotification> Notification_Upsert(AbstractNotification abstractNotification);

        public abstract PagedList<AbstractNotification> Notification_All(PageParam pageParam, string search , int UserId, int UserType);

        public abstract SuccessResult<AbstractNotification> Notification_IsReadUpdate(AbstractNotification abstractNotification);

        public abstract SuccessResult<AbstractNotification> Notification_Count(AbstractNotification abstractNotification);

    }
}
