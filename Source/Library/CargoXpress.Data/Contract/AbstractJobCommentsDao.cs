﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractJobCommentsDao
    {
        public abstract SuccessResult<AbstractJobComments> JobComments_Upsert(AbstractJobComments abstractJobComments);

        public abstract SuccessResult<AbstractJobComments> JobComments_ById(int Id);

        public abstract PagedList<AbstractJobComments> JobComments_ByJobId(PageParam pageParam, int JobId);

    }
}
