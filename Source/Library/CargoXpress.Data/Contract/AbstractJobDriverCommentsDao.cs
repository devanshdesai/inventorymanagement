﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractJobDriverCommentsDao
    {
        public abstract SuccessResult<AbstractJobDriverComments> JobDriverComments_Upsert(AbstractJobDriverComments abstractJobDriverComments);

        public abstract SuccessResult<AbstractJobDriverComments> JobDriverComments_ById(int Id);

        public abstract PagedList<AbstractJobDriverComments> JobDriverComments_All(PageParam pageParam, int JobId , int DriverId);


    }
}
