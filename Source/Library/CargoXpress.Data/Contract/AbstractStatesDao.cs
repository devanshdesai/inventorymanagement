﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractStatesDao
    {
        
        public abstract PagedList<AbstractStates> States_ByCountryId(PageParam pageParam, String Search, int CountryId);

    }
}
