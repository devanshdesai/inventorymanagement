﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractJobDocumentsDao
    {
        public abstract SuccessResult<AbstractJobDocuments> JobDocuments_Upsert(AbstractJobDocuments abstractJobDocuments);

        public abstract SuccessResult<AbstractJobDocuments> JobDocuments_ById(int Id);

        public abstract PagedList<AbstractJobDocuments> JobDocuments_ByJobId(PageParam pageParam, int JobId, int UserType);

    }
}
