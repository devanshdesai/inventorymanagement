﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Rushkar">
//     Copyright Rushkar Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CargoXpress.Data
{
    using Autofac;
    using CargoXpress.Data.Contract;
    using CargoXpress.Data.V1;

    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<V1.CarriersDao>().As<AbstractCarriersDao>().InstancePerDependency();
            builder.RegisterType<V1.CarrierTruckServicesDao>().As<AbstractCarrierTruckServicesDao>().InstancePerDependency();
            builder.RegisterType<V1.ShippersDao>().As<AbstractShippersDao>().InstancePerDependency();
            builder.RegisterType<V1.ShipperAddressesDao>().As<AbstractShipperAddressesDao>().InstancePerDependency();
            builder.RegisterType<V1.ShiperDocumentsDao>().As<AbstractShiperDocumentsDao>().InstancePerDependency();
            //builder.RegisterType<V1.CarrierDocumentsDao>().As<AbstractCarrierDocumentsDao>().InstancePerDependency();
            //builder.RegisterType<V1.CarrierPreferedDestinationsDao>().As<AbstractCarrierPreferedDestinationsDao>().InstancePerDependency();
            //builder.RegisterType<V1.DriversDao>().As<AbstractDriversDao>().InstancePerDependency();
            //builder.RegisterType<V1.DriverDocumentsDao>().As<AbstractDriverDocumentsDao>().InstancePerDependency();
            //builder.RegisterType<V1.TrucksDao>().As<AbstractTrucksDao>().InstancePerDependency();
            //builder.RegisterType<V1.TruckDocumentsDao>().As<AbstractTruckDocumentsDao>().InstancePerDependency();
            //builder.RegisterType<V1.TruckPrefferedDestinationsDao>().As<AbstractTruckPrefferedDestinationsDao>().InstancePerDependency();
            //builder.RegisterType<V1.CountriesDao>().As<AbstractCountriesDao>().InstancePerDependency();
            //builder.RegisterType<V1.StatesDao>().As<AbstractStatesDao>().InstancePerDependency();
            //builder.RegisterType<V1.JobStatusDao>().As<AbstractJobStatusDao>().InstancePerDependency();
            //builder.RegisterType<V1.DocumentTypeDao>().As<AbstractDocumentTypeDao>().InstancePerDependency();
            //builder.RegisterType<V1.LocationTypesDao>().As<AbstractLocationTypesDao>().InstancePerDependency();
            //builder.RegisterType<V1.TruckTypesDao>().As<AbstractTruckTypesDao>().InstancePerDependency();
            //builder.RegisterType<V1.TruckServicesDao>().As<AbstractTruckServicesDao>().InstancePerDependency();
            //builder.RegisterType<V1.AdminDao>().As<AbstractAdminDao>().InstancePerDependency();
            //builder.RegisterType<V1.JobDocumentsDao>().As<AbstractJobDocumentsDao>().InstancePerDependency();
            //builder.RegisterType<V1.JobCommentsDao>().As<AbstractJobCommentsDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterJobStatusDao>().As<AbstractMasterJobStatusDao>().InstancePerDependency();
            //builder.RegisterType<V1.JobDao>().As<AbstractJobDao>().InstancePerDependency();
            //builder.RegisterType<V1.JobDriverLocationDao>().As<AbstractJobDriverLocationDao>().InstancePerDependency();
            builder.RegisterType<V1.CarrierDocumentsDao>().As<AbstractCarrierDocumentsDao>().InstancePerDependency();
            builder.RegisterType<V1.CarrierPreferedDestinationsDao>().As<AbstractCarrierPreferedDestinationsDao>().InstancePerDependency();
            builder.RegisterType<V1.DriversDao>().As<AbstractDriversDao>().InstancePerDependency();
            builder.RegisterType<V1.DriverDocumentsDao>().As<AbstractDriverDocumentsDao>().InstancePerDependency();
            builder.RegisterType<V1.TrucksDao>().As<AbstractTrucksDao>().InstancePerDependency();
            builder.RegisterType<V1.TruckDocumentsDao>().As<AbstractTruckDocumentsDao>().InstancePerDependency();
            builder.RegisterType<V1.TruckPrefferedDestinationsDao>().As<AbstractTruckPrefferedDestinationsDao>().InstancePerDependency();
            builder.RegisterType<V1.CountriesDao>().As<AbstractCountriesDao>().InstancePerDependency();
            builder.RegisterType<V1.StatesDao>().As<AbstractStatesDao>().InstancePerDependency();
            builder.RegisterType<V1.JobStatusDao>().As<AbstractJobStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.DocumentTypeDao>().As<AbstractDocumentTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.LocationTypesDao>().As<AbstractLocationTypesDao>().InstancePerDependency();
            builder.RegisterType<V1.TruckTypesDao>().As<AbstractTruckTypesDao>().InstancePerDependency();
            builder.RegisterType<V1.TruckServicesDao>().As<AbstractTruckServicesDao>().InstancePerDependency();
            builder.RegisterType<V1.AdminDao>().As<AbstractAdminDao>().InstancePerDependency();
            builder.RegisterType<V1.JobDocumentsDao>().As<AbstractJobDocumentsDao>().InstancePerDependency();
            builder.RegisterType<V1.JobCommentsDao>().As<AbstractJobCommentsDao>().InstancePerDependency();
            builder.RegisterType<V1.JobBidsDao>().As<AbstractJobBidsDao>().InstancePerDependency();
            builder.RegisterType<V1.JobDao>().As<AbstractJobDao>().InstancePerDependency();
            builder.RegisterType<V1.JobDriverLocationDao>().As<AbstractJobDriverLocationDao>().InstancePerDependency();
            builder.RegisterType<V1.ShipperCardsDao>().As<AbstractShipperCardsDao>().InstancePerDependency();
            builder.RegisterType<V1.CarrierBankDetailsDao>().As<AbstractCarrierBankDetailsDao>().InstancePerDependency();
            builder.RegisterType<V1.JobActivityDao>().As<AbstractJobActivityDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterDriverCommentsDao>().As<AbstractMasterDriverCommentsDao>().InstancePerDependency();
            builder.RegisterType<V1.RouteMasterDao>().As<AbstractRouteMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.RouteChildDao>().As<AbstractRouteChildDao>().InstancePerDependency();
            builder.RegisterType<V1.JobDriverCommentsDao>().As<AbstractJobDriverCommentsDao>().InstancePerDependency(); base.Load(builder);
            builder.RegisterType<V1.MasterPricingPlansDao>().As<AbstractMasterPricingPlansDao>().InstancePerDependency(); base.Load(builder);
            builder.RegisterType<V1.UserTypeDao>().As<AbstractUserTypeDao>().InstancePerDependency(); base.Load(builder);
            builder.RegisterType<V1.JobDriverLocationDao>().As<AbstractJobDriverLocationDao>().InstancePerDependency(); base.Load(builder);
            builder.RegisterType<V1.CarrierPricingPlanDao>().As<AbstractCarrierPricingPlanDao>().InstancePerDependency(); base.Load(builder);
            builder.RegisterType<V1.SupportDao>().As<AbstractSupportDao>().InstancePerDependency();
            builder.RegisterType<V1.SupportReplyDao>().As<AbstractSupportReplyDao>().InstancePerDependency();
            builder.RegisterType<V1.PaymentMasterDao>().As<AbstractPaymentMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.NotificationDao>().As<AbstractNotificationDao>().InstancePerDependency();
            builder.RegisterType<V1.JobBillOfLoadingDao>().As<AbstractJobBillOfLoadingDao>().InstancePerDependency();
            builder.RegisterType<V1.JobRatingsDao>().As<AbstractJobRatingsDao>().InstancePerDependency();
            builder.RegisterType<V1.JobTruckDriversDao>().As<AbstractJobTruckDriversDao>().InstancePerDependency();
            builder.RegisterType<V1.JobBidsFinanceDao>().As<AbstractJobBidsFinanceDao>().InstancePerDependency();

        }
    }
}
