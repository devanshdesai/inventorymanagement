﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CargoXpress.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        #region Carriers
        public const string Carriers_Upsert = "Carriers_Upsert";
        public const string Carriers_ById = "Carriers_ById";
        public const string Carriers_All = "Carriers_All";
        public const string Carriers_Login = "Carriers_Login";
        public const string Carriers_ActInact = "Carriers_ActInact";
        public const string Carriers_Approve = "Carriers_Approve";
        public const string Carriers_ChangePassword = "Carriers_ChangePassword";
        public const string Carriers_EmailVerify = "Carriers_EmailVerify";
        public const string Carriers_MobileVerify = "Carriers_MobileVerify";
        public const string Carriers_Logout = "Carriers_Logout";
        public const string Carriers_ResetPassword = "Carriers_ResetPassword";
        public const string Carriers_ProfileUpdate = "Carriers_ProfileUpdate";
        #endregion

        #region JobBidsFinance

        public const string JobBidsFinance_All = "JobBidsFinance_All";
        public const string JobBidsFinance_ById = "JobBidsFinance_ById";
        public const string JobBidsFinance_Upsert = "JobBidsFinance_Upsert";
        public const string JobBidsFinance_Delete = "JobBidsFinance_Delete";

        #endregion

        #region Notification
        public const string Notification_All = "Notification_All";
        public const string Notification_Upsert = "Notification_Upsert";
        public const string Notification_Count = "Notification_Count";
        public const string Notification_IsReadUpdate = "Notification_IsReadUpdate";
        #endregion

        #region JobBillOfLoading
        public const string JobBillOfLoading_All = "JobBillOfLoading_All";
        public const string JobBillOfLoading_Delete = "JobBillOfLoading_Delete";
        public const string JobBillOfLoading_Upsert = "JobBillOfLoading_Upsert";
        #endregion

        #region CarrierTruckServices
        public const string CarrierTruckServices_Upsert = "CarrierTruckServices_Upsert";
        public const string CarrierTruckServices_ById = "CarrierTruckServices_ById";
        public const string CarrierTruckServices_All = "CarrierTruckServices_All";
        public const string CarrierTruckServices_Delete = "CarrierTruckServices_Delete";
        #endregion

        #region MasterJobStatus
        public const string MasterJobStatus_All = "MasterJobStatus_All";
       #endregion
        

        #region Shippers
        public const string Shippers_Upsert = "Shippers_Upsert";
        public const string Shippers_ById = "Shippers_ById";
        public const string Shippers_All = "Shippers_All";
        public const string Shippers_Login = "Shippers_Login";
        public const string Shippers_ActInact = "Shippers_ActInact";
        public const string Shippers_ChangePassword = "Shippers_ChangePassword";
        public const string Shippers_EmailVerified = "Shippers_EmailVerified";
        public const string Shippers_MobileVerified = "Shippers_MobileVerified";
        public const string Shippers_Logout = "Shippers_Logout";
        public const string Shippers_ProfileUpdate = "Shippers_ProfileUpdate";
        #endregion

        #region ShiperDocuments
        public const string ShipperDocuments_Upsert = "ShipperDocuments_Upsert";
        public const string ShiperDocuments_All = "ShipperDocuments_All";
        public const string ShiperDocuments_ById = "ShipperDocuments_ById";
        #endregion

        #region ShipperAddresses
        public const string ShipperAddresses_Upsert = "ShipperAddresses_Upsert";
        public const string ShipperAddresses_All = "ShipperAddresses_All";
        public const string ShipperAddresses_ById = "ShipperAddresses_ById";
        #endregion

        #region CarrierDocuments
        public const string CarrierDocuments_Upsert = "CarrierDocuments_Upsert";
        public const string CarrierDocuments_ById = "CarrierDocuments_ById";
        public const string CarrierDocuments_All = "CarrierDocuments_All";
        #endregion
        //add by krinal
        #region MasterDriverComments
        public const string MasterDriverComments_All = "MasterDriverComments_All";
        #endregion
        //add by krinal

        #region JobDriverComments
        public const string JobDriverComments_All = "JobDriverComments_All";
        public const string JobDriverComments_ById = "JobDriverComments_ById";
        public const string JobDriverComments_Upsert = "JobDriverComments_Upsert";
        #endregion

        #region CarrierPreferedDestinations
        public const string CarrierPreferedDestinations_Upsert = "CarrierPreferedDestinations_Upsert";
        public const string CarrierPreferedDestinations_ById = "CarrierPreferedDestinations_ById";
        public const string CarrierPreferedDestinations_All = "CarrierPreferedDestinations_All";
        #endregion

        #region Drivers
        public const string Drivers_Upsert = "Drivers_Upsert";
        public const string Drivers_All = "Drivers_All";
        public const string Drivers_ById = "Drivers_ById";
        public const string Drivers_Login = "Drivers_Login";
        public const string Drivers_ActInact = "Drivers_ActInact";
        public const string Drivers_ChangePassword = "Drivers_ChangePassword";
        public const string Drivers_EmailVerified = "Drivers_EmailVerified";
        public const string Drivers_MobileVerify = "Drivers_MobileVerify";
        public const string Drivers_Logout = "Drivers_Logout";
        public const string Drivers_ProfileUpdate = "Drivers_ProfileUpdate";
        #endregion

        #region DriverDocuments
        public const string DriverDocuments_Upsert = "DriverDocuments_Upsert";
        public const string DriverDocuments_ById = "DriverDocuments_ById";
        public const string DriverDocuments_All = "DriverDocuments_All";
        #endregion

        #region Trucks
        public const string Trucks_Upsert = "Trucks_Upsert";
        public const string Trucks_All = "Trucks_All";
        public const string Trucks_ById = "Trucks_ById";
        public const string Trucks_ActInact = "Trucks_ActInact";
        public const string Trucks_JobActInact = "Trucks_JobActInact";
        #endregion

        #region TruckDocuments
        public const string TruckDocuments_Upsert = "TruckDocuments_Upsert";
        public const string TruckDocuments_All = "TruckDocuments_All";
        public const string TruckDocuments_ById = "TruckDocuments_ById";
        #endregion

        #region TruckPrefferedDestinations
        public const string TruckPrefferedDestinations_Upsert = "TruckPrefferedDestinations_Upsert";
        public const string TruckPrefferedDestinations_ById = "TruckPrefferedDestinations_ById";
        public const string TruckPrefferedDestinations_All = "TruckPrefferedDestinations_All";
        #endregion

        #region Countries
        public const string Countries_All = "Countries_All";
        #endregion

        #region States
        public const string States_ByCountryId = "States_ByCountryId";
        #endregion

        #region JobStatus
        public const string JobStatus_All = "JobStatus_All";
        #endregion

        #region DocumentType
        public const string DocumentType_All = "DocumentType_All";
        #endregion

        #region LocationTypes
        public const string LocationTypes_Upsert = "LocationTypes_Upsert";
        public const string LocationTypes_ById = "LocationTypes_ById";
        public const string LocationTypes_All = "LocationTypes_All";
        #endregion

        #region TruckTypes
        public const string TruckTypes_Upsert = "TruckTypes_Upsert";
        public const string TruckTypes_ById = "TruckTypes_ById";
        public const string TruckTypes_All = "TruckTypes_All";
        #endregion

        #region TruckServices
        public const string TruckServices_Upsert = "TruckServices_Upsert";
        public const string TruckServices_ById = "TruckServices_ById";
        public const string TruckServices_All = "TruckServices_All";
        public const string TruckServices_ByParentId = "TruckServices_ByParentId";
        #endregion

        #region Admin
        public const string Admin_ById = "Admin_ById";
        public const string Admin_Login = "Admin_Login";
        public const string Admin_ChangePassword = "Admin_ChangePassword";
        public const string Admin_Logout = "Admin_Logout";
        #endregion

        #region JobDocuments
        public const string JobDocuments_Upsert = "JobDocuments_Upsert";
        public const string JobDocuments_ById = "JobDocuments_ById";
        public const string JobDocuments_ByJobId = "JobDocuments_ByJobId";
        #endregion

        #region JobComments
        public const string JobComments_Upsert = "JobComments_Upsert";
        public const string JobComments_ById = "JobComments_ById";
        public const string JobComments_ByJobId = "JobComments_ByJobId";
        #endregion

        #region JobBids
        public const string JobBids_Upsert = "JobBids_Upsert";
        public const string JobBids_ById = "JobBids_ById";
        public const string JobBids_ByJobId = "JobBids_ByJobId";
        public const string JobBidStatus_Update = "JobBidStatus_Update";
        #endregion
        #region JobActivity

        public const string JobActivity_ByJobId = "JobActivity_ByJobId";
        #endregion

        #region Job
        public const string Job_ById = "Job_ById";
        public const string Job_All = "Job_All";
        public const string Job_AssignDriver = "Job_AssignDriver";
        public const string Job_AssignTruck = "Job_AssignTruck";
        public const string Job_CarrierJobEndDate = "Job_CarrierJobEndDate";
        public const string Job_CarrierJobStartDate = "Job_CarrierJobStartDate";
        public const string Job_JobStatus = "Job_JobStatus";
        public const string Job_Upsert = "Job_Upsert";
        public const string Job_AcceptBid = "Job_AcceptBid";
        #endregion

        #region JobDriverLocation
        public const string JobDriverLocation_Upsert = "JobDriverLocation_Upsert";
        public const string JobDriverLocations_ById = "JobDriverLocations_ById";
        public const string JobDriverLocation_ByJobId = "JobDriverLocation_ByJobId";
        #endregion

        #region JobRatings
        public const string JobRatings_Upsert = "JobRatings_Upsert";
        public const string JobRatings_ById = "JobRatings_ById";
        public const string JobRatings_ByJobId = "JobRatings_ByJobId";
        #endregion

        #region ShipperCards
        public const string ShipperCards_Upsert = "ShipperCards_Upsert";
        public const string ShipperCards_ById = "ShipperCards_ById";
        public const string ShipperCards_ByShipperId = "ShipperCards_ByShipperId";
        public const string ShipperCards_Delete = "ShipperCards_Delete";
        public const string ShipperCards_IsDefault = "ShipperCards_IsDefault";
        #endregion

        #region CarrierBankDetails
        public const string CarrierBankDetails_Upsert = "CarrierBankDetails_Upsert";
        public const string CarrierBankDetails_ById = "CarrierBankDetails_ById";
        public const string CarrierBankDetails_ByCarrierId = "CarrierBankDetails_ByCarrierId";
        public const string CarrierBankDetails_Delete = "CarrierBankDetails_Delete";
        public const string CarrierBankDetails_IsDefault = "CarrierBankDetails_IsDefault";
        #endregion


        #region RouteMaster
        public const string RouteMaster_Upsert = "RouteMaster_Upsert";
        public const string RouteMaster_ById = "RouteMaster_ById";
        public const string RouteMaster_ByJobId = "RouteMaster_ByJobId";
        public const string RouteMaster_Delete = "RouteMaster_Delete";
        #endregion

        #region RouteMaster
        public const string RouteChild_Upsert = "RouteChild_Upsert";
        public const string RouteChild_ById = "RouteChild_ById";
        public const string RouteChild_ByRouteMasterId = "RouteChild_ByRouteMasterId";
        public const string RouteChild_Delete = "RouteChild_Delete";
        public const string CarrierPreferedDestinations_Delete = "CarrierPreferedDestinations_Delete";
        #endregion

        #region MasterPricingPlans
        public const string MasterPricingPlans_Upsert = "MasterPricingPlans_Upsert";
        public const string MasterPricingPlans_ById = "MasterPricingPlans_ById";
        public const string MasterPricingPlans_All = "MasterPricingPlans_All";
        public const string MasterPricingPlans_ActInact = "MasterPricingPlans_ActInact";
        #endregion

        #region UserType
        public const string UserType_All = "UserType_All";
        #endregion

        #region CarrierPricingPlan
        public const string CarrierPricingPlan_Upsert = "CarrierPricingPlan_Upsert";
        public const string CarrierPricingPlan_ByCarrierId = "CarrierPricingPlan_ByCarrierId";
        public const string CarrierPricingPlan_Delete = "CarrierPricingPlan_Delete";
        #endregion

        #region Support
        public const string Support_Upsert = "Support_Upsert";
        public const string Support_ActInact = "Support_ActInact";
        public const string Support_Close = "Support_Close ";
        public const string Support_All = "Support_All";
        public const string Support_ById = "Support_ById";
        #endregion

        #region SupportReply
        public const string SupportReply_Upsert = "SupportReply_Upsert";
        public const string SupportReply_BySupportId = "SupportReply_BySupportId ";
        #endregion

        #region PaymentMaster
        public const string PaymentMaster_Upsert = "PaymentMaster_Upsert";
        public const string PaymentMaster_ById = "PaymentMaster_ById";
        public const string PaymentMaster_All = "PaymentMaster_All";
        public const string TransactionReport = "TransactionReport";
        public const string PaymentMaster_Delete = "PaymentMaster_Delete";
        public const string PaymentMaster_RecievedNotRecieved = "PaymentMaster_RecievedNotRecieved";
        #endregion


        #region JobTruckDrivers
        public const string JobTruckDrivers_Upsert = "JobTruckDrivers_Upsert";
        public const string JobTruckDrivers_ById = "JobTruckDrivers_ById";
        public const string JobTruckDrivers_ByJobId = "JobTruckDrivers_ByJobId";
        public const string JobTruckDrivers_Delete = "JobTruckDrivers_Delete";
        #endregion

        public const string TransactionReport_ADVANCED = "TransactionReport_ADVANCED";
    }
}
