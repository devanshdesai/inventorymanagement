﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CargoXpress.Services
{
    using Autofac;    
    using Data;
    using CargoXpress.Services.Contract;

    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {           
            builder.RegisterModule<DataModule>();

            builder.RegisterType<V1.CarriersServices>().As<AbstractCarriersServices>().InstancePerDependency();
            builder.RegisterType<V1.ShippersServices>().As<AbstractShippersServices>().InstancePerDependency();
            builder.RegisterType<V1.CarrierTruckServicesServices>().As<AbstractCarrierTruckServicesServices>().InstancePerDependency();
            builder.RegisterType<V1.ShipperAddressesService>().As<AbstractShipperAddressesService>().InstancePerDependency();
            builder.RegisterType<V1.ShiperDocumentsServices>().As<AbstractShiperDocumentsServices>().InstancePerDependency();
            builder.RegisterType<V1.CarrierDocumentsServices>().As<AbstractCarrierDocumentsServices>().InstancePerDependency();
            builder.RegisterType<V1.CarrierPreferedDestinationsServices>().As<AbstractCarrierPreferedDestinationsServices>().InstancePerDependency();
            builder.RegisterType<V1.DriversServices>().As<AbstractDriversService>().InstancePerDependency();
            builder.RegisterType<V1.DriverDocumentsServices>().As<AbstractDriverDocumentsServices>().InstancePerDependency();
            builder.RegisterType<V1.TrucksServices>().As<AbstractTrucksService>().InstancePerDependency();
            builder.RegisterType<V1.TruckDocumentsServices>().As<AbstractTruckDocumentsService>().InstancePerDependency();
            builder.RegisterType<V1.TruckPrefferedDestinationsServices>().As<AbstractTruckPrefferedDestinationsServices>().InstancePerDependency();
            builder.RegisterType<V1.CountriesServices>().As<AbstractCountriesService>().InstancePerDependency();
            builder.RegisterType<V1.StatesServices>().As<AbstractStatesServices>().InstancePerDependency();
            builder.RegisterType<V1.JobStatusServices>().As<AbstractJobStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.DocumentTypeServices>().As<AbstractDocumentTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.LocationTypesServices>().As<AbstractLocationTypesServices>().InstancePerDependency();
            builder.RegisterType<V1.TruckTypesServices>().As<AbstractTruckTypesServices>().InstancePerDependency();
            builder.RegisterType<V1.TruckServicesServices>().As<AbstractTruckServicesServices>().InstancePerDependency();
            builder.RegisterType<V1.AdminServices>().As<AbstractAdminServices>().InstancePerDependency();
            builder.RegisterType<V1.JobCommentsServices>().As<AbstractJobCommentsServices>().InstancePerDependency();
            builder.RegisterType<V1.JobDocumentsServices>().As<AbstractJobDocumentsServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterJobStatusServices>().As<AbstractMasterJobStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.JobBidsServices>().As<AbstractJobBidsServices>().InstancePerDependency();
            builder.RegisterType<V1.JobServices>().As<AbstractJobServices>().InstancePerDependency();
            builder.RegisterType<V1.JobRatingsServices>().As<AbstractJobRatingsServices>().InstancePerDependency();
            builder.RegisterType<V1.ShipperCardsServices>().As<AbstractShipperCardsServices>().InstancePerDependency();
            builder.RegisterType<V1.CarrierBankDetailsServices>().As<AbstractCarrierBankDetailsServices>().InstancePerDependency();
            builder.RegisterType<V1.JobActivityServices>().As<AbstractJobActivityServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterDriverCommentsServices>().As<AbstractMasterDriverCommentsServices>().InstancePerDependency();
            builder.RegisterType<V1.JobDriverCommentsServices>().As<AbstractJobDriverCommentsServices>().InstancePerDependency();
            builder.RegisterType<V1.RouteMasterServices>().As<AbstractRouteMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.RouteChildServices>().As<AbstractRouteChildServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterPricingPlansServices>().As<AbstractMasterPricingPlansServices>().InstancePerDependency();
            builder.RegisterType<V1.UserTypeServices>().As<AbstractUserTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.JobDriverLocationServices>().As<AbstractJobDriverLocationServices>().InstancePerDependency();
            builder.RegisterType<V1.CarrierPricingPlanServices>().As<AbstractCarrierPricingPlanServices>().InstancePerDependency();
            builder.RegisterType<V1.SupportServices>().As<AbstractSupportServices>().InstancePerDependency();
            builder.RegisterType<V1.SupportReplyServices>().As<AbstractSupportReplyServices>().InstancePerDependency();
            builder.RegisterType<V1.PaymentMasterServices>().As<AbstractPaymentMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.NotificationServices>().As<AbstractNotificationServices>().InstancePerDependency();
            builder.RegisterType<V1.JobBillOfLoadingServices>().As<AbstractJobBillOfLoadingServices>().InstancePerDependency();
            builder.RegisterType<V1.JobTruckDriversServices>().As<AbstractJobTruckDriversServices>().InstancePerDependency();
            builder.RegisterType<V1.JobBidsFinanceServices>().As<AbstractJobBidsFinanceServices>().InstancePerDependency();
            base.Load(builder);
        }
    }
}
