﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class TruckDocumentsServices : AbstractTruckDocumentsService
    {
        private AbstractTruckDocumentsDao abstractTruckDocumentsDao;

        public TruckDocumentsServices(AbstractTruckDocumentsDao abstractTruckDocumentsDao)
        {
            this.abstractTruckDocumentsDao = abstractTruckDocumentsDao;
        }

        public override SuccessResult<AbstractTruckDocuments> TruckDocuments_Upsert(AbstractTruckDocuments abstractTruckDocuments)
        {
            return this.abstractTruckDocumentsDao.TruckDocuments_Upsert(abstractTruckDocuments);
        }

        public override PagedList<AbstractTruckDocuments> TruckDocuments_All(PageParam pageParam, int TruckId)
        {
            return this.abstractTruckDocumentsDao.TruckDocuments_All(pageParam, TruckId);
        }

        public override SuccessResult<AbstractTruckDocuments> TruckDocuments_ById(int Id)
        {
            return this.abstractTruckDocumentsDao.TruckDocuments_ById(Id);
        }

    }
}
