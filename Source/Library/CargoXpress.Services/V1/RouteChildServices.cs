﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class RouteChildServices : AbstractRouteChildServices
    {
        private AbstractRouteChildDao abstractRouteChildDao;

        public RouteChildServices(AbstractRouteChildDao abstractRouteChildDao)
        {
            this.abstractRouteChildDao = abstractRouteChildDao;
        }

        public override SuccessResult<AbstractRouteChild> RouteChild_Upsert(AbstractRouteChild abstractRouteChild)
        {
            return abstractRouteChildDao.RouteChild_Upsert(abstractRouteChild);
        }

        public override SuccessResult<AbstractRouteChild> RouteChild_ById(int Id)
        {
            return abstractRouteChildDao.RouteChild_ById(Id);
        }

        public override PagedList<AbstractRouteChild> RouteChild_ByRouteMasterId(PageParam pageParam, string search, int RouteMasterId)
        {
            return this.abstractRouteChildDao.RouteChild_ByRouteMasterId(pageParam, search, RouteMasterId);
        }

        

        public override bool RouteChild_Delete(int Id)
        {
            return abstractRouteChildDao.RouteChild_Delete(Id);
        }

    }
}
