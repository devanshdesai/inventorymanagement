﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class StatesServices : AbstractStatesServices
    {
        private AbstractStatesDao abstractStatesDao;

        public StatesServices(AbstractStatesDao abstractStatesDao)
        {
            this.abstractStatesDao = abstractStatesDao;
        }

      
        public override PagedList<AbstractStates> States_ByCountryId(PageParam pageParam, String Search, int CountryId)
        {
            return this.abstractStatesDao.States_ByCountryId(pageParam, Search, CountryId);
        }

    }
}
