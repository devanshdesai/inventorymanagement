﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobTruckDriversServices : AbstractJobTruckDriversServices
    {
        private AbstractJobTruckDriversDao abstractJobTruckDriversDao;

        public JobTruckDriversServices(AbstractJobTruckDriversDao abstractJobTruckDriversDao)
        {
            this.abstractJobTruckDriversDao = abstractJobTruckDriversDao;
        }

        public override SuccessResult<AbstractJobTruckDrivers> JobTruckDrivers_Upsert(AbstractJobTruckDrivers abstractJobTruckDrivers)
        {
            return abstractJobTruckDriversDao.JobTruckDrivers_Upsert(abstractJobTruckDrivers);
        }

        public override SuccessResult<AbstractJobTruckDrivers> JobTruckDrivers_ById(int Id)
        {
            return abstractJobTruckDriversDao.JobTruckDrivers_ById(Id);
        }

        public override PagedList<AbstractJobTruckDrivers> JobTruckDrivers_ByJobId(PageParam pageParam, string search, int JobId, int DriverId = 0, int TruckId = 0)
        {
            return this.abstractJobTruckDriversDao.JobTruckDrivers_ByJobId(pageParam, search, JobId, DriverId, TruckId);
        }

        public override bool JobTruckDrivers_Delete(int Id)
        {
            return abstractJobTruckDriversDao.JobTruckDrivers_Delete(Id);
        }

    }
}
