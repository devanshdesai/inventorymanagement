﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CarrierPricingPlanServices : AbstractCarrierPricingPlanServices
    {
        private AbstractCarrierPricingPlanDao abstractCarrierPricingPlanDao;

        public CarrierPricingPlanServices(AbstractCarrierPricingPlanDao abstractCarrierPricingPlanDao)
        {
            this.abstractCarrierPricingPlanDao = abstractCarrierPricingPlanDao;
        }

        public override SuccessResult<AbstractCarrierPricingPlan> CarrierPricingPlan_Upsert(AbstractCarrierPricingPlan abstractCarrierPricingPlan)
        {
            return this.abstractCarrierPricingPlanDao.CarrierPricingPlan_Upsert(abstractCarrierPricingPlan);
        }


        public override PagedList<AbstractCarrierPricingPlan> CarrierPricingPlan_ByCarrierId(PageParam pageParam, string search, int CarrierId, int UserType)
        {
            return this.abstractCarrierPricingPlanDao.CarrierPricingPlan_ByCarrierId(pageParam,search, CarrierId, UserType);
        }

        public override SuccessResult<AbstractCarrierPricingPlan> CarrierPricingPlan_Delete(int Id)
        {
            return this.abstractCarrierPricingPlanDao.CarrierPricingPlan_Delete(Id);
        }

        

       
    }
}
