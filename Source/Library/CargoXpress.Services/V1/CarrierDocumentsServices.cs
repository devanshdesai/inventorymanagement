﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CarrierDocumentsServices : AbstractCarrierDocumentsServices
    {
        private AbstractCarrierDocumentsDao abstractCarrierDocumentsDao;

        public CarrierDocumentsServices(AbstractCarrierDocumentsDao abstractCarrierDocumentsDao)
        {
            this.abstractCarrierDocumentsDao = abstractCarrierDocumentsDao;
        }

        public override SuccessResult<AbstractCarrierDocuments> CarrierDocuments_Upsert(AbstractCarrierDocuments abstractCarrierDocuments)
        {
            return abstractCarrierDocumentsDao.CarrierDocuments_Upsert(abstractCarrierDocuments);
        }

        public override SuccessResult<AbstractCarrierDocuments> CarrierDocuments_ById(int Id)
        {
            return abstractCarrierDocumentsDao.CarrierDocuments_ById(Id);
        }

        public override PagedList<AbstractCarrierDocuments> CarrierDocuments_All(PageParam pageParam, int CarrierId)
        {
            return abstractCarrierDocumentsDao.CarrierDocuments_All(pageParam,CarrierId);
        }

    }
}
