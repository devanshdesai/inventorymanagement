﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class ShipperCardsServices : AbstractShipperCardsServices
    {
        private AbstractShipperCardsDao abstractShipperCardsDao;

        public ShipperCardsServices(AbstractShipperCardsDao abstractShipperCardsDao)
        {
            this.abstractShipperCardsDao = abstractShipperCardsDao;
        }

        public override SuccessResult<AbstractShipperCards> ShipperCards_Upsert(AbstractShipperCards abstractShipperCards)
        {
            return this.abstractShipperCardsDao.ShipperCards_Upsert(abstractShipperCards);
        }

        public override SuccessResult<AbstractShipperCards> ShipperCards_ById(int Id)
        {
            return this.abstractShipperCardsDao.ShipperCards_ById(Id);
        }

        public override PagedList<AbstractShipperCards> ShipperCards_ByCarrierId(PageParam pageParam, string search, int ShipperId)
        {
            return this.abstractShipperCardsDao.ShipperCards_ByShipperId(pageParam,search, ShipperId);
        }

        public override SuccessResult<AbstractShipperCards> ShipperCards_Delete(AbstractShipperCards abstractShipperCards)
        {
            return this.abstractShipperCardsDao.ShipperCards_Delete(abstractShipperCards);
        }

        public override SuccessResult<AbstractShipperCards> ShipperCards_IsDefault(int Id)
        {
            return this.abstractShipperCardsDao.ShipperCards_IsDefault(Id);
        }

       
    }
}
