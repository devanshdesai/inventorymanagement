﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class MasterJobStatusServices : AbstractMasterJobStatusServices
    {
        private AbstractMasterJobStatusDao abstractMasterJobStatusDao;

        public MasterJobStatusServices(AbstractMasterJobStatusDao abstractMasterJobStatusDao)
        {
            this.abstractMasterJobStatusDao = abstractMasterJobStatusDao;
        }

        public override PagedList<AbstractMasterJobStatus> MasterJobStatus_All(PageParam pageParam, String search, int JobStatusTypeId)
        {
            return this.abstractMasterJobStatusDao.MasterJobStatus_All(pageParam, search, JobStatusTypeId);
        }

    }
}
