﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobDocumentsServices : AbstractJobDocumentsServices
    {
        private AbstractJobDocumentsDao abstractJobDocumentsDao;

        public JobDocumentsServices(AbstractJobDocumentsDao abstractJobDocumentsDao)
        {
            this.abstractJobDocumentsDao = abstractJobDocumentsDao;
        }

        public override SuccessResult<AbstractJobDocuments> JobDocuments_Upsert(AbstractJobDocuments abstractJobDocuments)
        {
            return abstractJobDocumentsDao.JobDocuments_Upsert(abstractJobDocuments);
        }

        public override SuccessResult<AbstractJobDocuments> JobDocuments_ById(int Id)
        {
            return abstractJobDocumentsDao.JobDocuments_ById(Id);
        }

        public override PagedList<AbstractJobDocuments> JobDocuments_ByJobId(PageParam pageParam, int JobId,int UserType)
        {
            return this.abstractJobDocumentsDao.JobDocuments_ByJobId(pageParam, JobId, UserType);
        }

    }
}
