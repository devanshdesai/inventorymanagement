﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobStatusServices : AbstractJobStatusServices
    {
        private AbstractJobStatusDao abstractJobStatusDao;

        public JobStatusServices(AbstractJobStatusDao abstractJobStatusDao)
        {
            this.abstractJobStatusDao = abstractJobStatusDao;
        }

      
        public override PagedList<AbstractJobStatus> JobStatus_All(PageParam pageParam, String Search)
        {
            return this.abstractJobStatusDao.JobStatus_All(pageParam, Search);
        }

    }
}
