﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobCommentsServices : AbstractJobCommentsServices
    {
        private AbstractJobCommentsDao abstractJobCommentsDao;

        public JobCommentsServices(AbstractJobCommentsDao abstractJobCommentsDao)
        {
            this.abstractJobCommentsDao = abstractJobCommentsDao;
        }

        public override SuccessResult<AbstractJobComments> JobComments_Upsert(AbstractJobComments abstractJobComments)
        {
            return abstractJobCommentsDao.JobComments_Upsert(abstractJobComments);
        }

        public override SuccessResult<AbstractJobComments> JobComments_ById(int Id)
        {
            return abstractJobCommentsDao.JobComments_ById(Id);
        }

        public override PagedList<AbstractJobComments> JobComments_ByJobId(PageParam pageParam, int JobId)
        {
            return this.abstractJobCommentsDao.JobComments_ByJobId(pageParam, JobId);
        }

    }
}
