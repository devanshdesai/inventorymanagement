﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class TruckServicesServices : AbstractTruckServicesServices
    {
        private AbstractTruckServicesDao abstractTruckServicesDao;

        public TruckServicesServices(AbstractTruckServicesDao abstractTruckServicesDao)
        {
            this.abstractTruckServicesDao = abstractTruckServicesDao;
        }

        public override SuccessResult<AbstractTruckServices> TruckServices_Upsert(AbstractTruckServices abstractTruckServices)
        {
            return abstractTruckServicesDao.TruckServices_Upsert(abstractTruckServices);
        }

        public override SuccessResult<AbstractTruckServices> TruckServices_ById(int Id)
        {
            return abstractTruckServicesDao.TruckServices_ById(Id);
        }

        public override PagedList<AbstractTruckServices> TruckServices_All(PageParam pageParam, String Search, int ParentId)
        {
            return this.abstractTruckServicesDao.TruckServices_All(pageParam, Search, ParentId);
        }

        public override PagedList<AbstractTruckServices> TruckServices_ByParentId(PageParam pageParam, String Search)
        {
            return this.abstractTruckServicesDao.TruckServices_ByParentId(pageParam, Search);
        }

    }
}
