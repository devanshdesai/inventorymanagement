﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class AdminServices : AbstractAdminServices
    {
        private AbstractAdminDao abstractAdminDao;

        public AdminServices(AbstractAdminDao abstractAdminDao)
        {
            this.abstractAdminDao = abstractAdminDao;
        }

        public override SuccessResult<AbstractAdmin> Admin_ById(int Id)
        {
            return this.abstractAdminDao.Admin_ById(Id);
        }

        public override SuccessResult<AbstractAdmin> Admin_Login(AbstractAdmin abstractAdmin)
        {
            return this.abstractAdminDao.Admin_Login(abstractAdmin);
        }

        public override bool Admin_ChangePassword(AbstractAdmin abstractAdmin)
        {
            return this.abstractAdminDao.Admin_ChangePassword(abstractAdmin);
        }
       
        public override bool Admin_Logout(int Id)    
        {
            return this.abstractAdminDao.Admin_Logout(Id);
        }

     
    }
}
