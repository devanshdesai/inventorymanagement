﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CarrierBankDetailsServices : AbstractCarrierBankDetailsServices
    {
        private AbstractCarrierBankDetailsDao abstractCarrierBankDetailsDao;

        public CarrierBankDetailsServices(AbstractCarrierBankDetailsDao abstractCarrierBankDetailsDao)
        {
            this.abstractCarrierBankDetailsDao = abstractCarrierBankDetailsDao;
        }

        public override SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_Upsert(AbstractCarrierBankDetails abstractCarrierBankDetails)
        {
            return this.abstractCarrierBankDetailsDao.CarrierBankDetails_Upsert(abstractCarrierBankDetails);
        }

        public override SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_ById(int Id)
        {
            return this.abstractCarrierBankDetailsDao.CarrierBankDetails_ById(Id);
        }

        public override PagedList<AbstractCarrierBankDetails> CarrierBankDetails_ByCarrierId(PageParam pageParam, string search, int CarrierId)
        {
            return this.abstractCarrierBankDetailsDao.CarrierBankDetails_ByCarrierId(pageParam,search, CarrierId);
        }

        public override SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_Delete(AbstractCarrierBankDetails abstractCarrierBankDetails)
        {
            return this.abstractCarrierBankDetailsDao.CarrierBankDetails_Delete(abstractCarrierBankDetails);
        }

        public override SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_IsDefault(int Id)
        {
            return this.abstractCarrierBankDetailsDao.CarrierBankDetails_IsDefault(Id);
        }

       
    }
}
