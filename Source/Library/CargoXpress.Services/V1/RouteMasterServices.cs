﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class RouteMasterServices : AbstractRouteMasterServices
    {
        private AbstractRouteMasterDao abstractRouteMasterDao;

        public RouteMasterServices(AbstractRouteMasterDao abstractRouteMasterDao)
        {
            this.abstractRouteMasterDao = abstractRouteMasterDao;
        }

        public override SuccessResult<AbstractRouteMaster> RouteMaster_Upsert(AbstractRouteMaster abstractRouteMaster)
        {
            return abstractRouteMasterDao.RouteMaster_Upsert(abstractRouteMaster);
        }

        public override SuccessResult<AbstractRouteMaster> RouteMaster_ById(int Id)
        {
            return abstractRouteMasterDao.RouteMaster_ById(Id);
        }

        public override PagedList<AbstractRouteMaster> RouteMaster_ByJobId(PageParam pageParam, string search, int JobId,int JobTruckDriverId=0)
        {
            return this.abstractRouteMasterDao.RouteMaster_ByJobId(pageParam, search, JobId, JobTruckDriverId);
        }

        

        public override bool RouteMaster_Delete(int Id)
        {
            return abstractRouteMasterDao.RouteMaster_Delete(Id);
        }

    }
}
