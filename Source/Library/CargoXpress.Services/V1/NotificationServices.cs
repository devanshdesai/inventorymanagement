﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class NotificationServices : AbstractNotificationServices
    {
        private AbstractNotificationDao abstractNotificationDao;

        public NotificationServices(AbstractNotificationDao abstractNotificationDao)
        {
            this.abstractNotificationDao = abstractNotificationDao;
        }

        public override SuccessResult<AbstractNotification> Notification_Upsert(AbstractNotification abstractNotification)
        {
            return abstractNotificationDao.Notification_Upsert(abstractNotification);
        }

        public override PagedList<AbstractNotification> Notification_All(PageParam pageParam, string search, int UserId, int UserType)
        {
            return abstractNotificationDao.Notification_All(pageParam, search,UserId, UserType);
        }

        public override SuccessResult<AbstractNotification> Notification_Count(AbstractNotification abstractNotification)
        {
            return abstractNotificationDao.Notification_Count(abstractNotification);
        }

        public override SuccessResult<AbstractNotification> Notification_IsReadUpdate(AbstractNotification abstractNotification)
        {
            return abstractNotificationDao.Notification_IsReadUpdate(abstractNotification);
        }

    }
}
