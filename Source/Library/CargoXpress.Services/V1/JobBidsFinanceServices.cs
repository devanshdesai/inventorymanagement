﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Services.V1
{
    public class JobBidsFinanceServices : AbstractJobBidsFinanceServices
    {
        private AbstractJobBidsFinanceDao abstractJobBidsFinanceDao = null;

        public JobBidsFinanceServices(AbstractJobBidsFinanceDao abstractJobBidsFinanceDao)
        {
            this.abstractJobBidsFinanceDao=abstractJobBidsFinanceDao;
        }

        

        public override PagedList<AbstractJobBidsFinance> JobBidsFinance_All(PageParam pageParam, string search,int JobBidId)
        {
            return abstractJobBidsFinanceDao.JobBidsFinance_All(pageParam, search,JobBidId);
        }


        public override SuccessResult<AbstractJobBidsFinance> JobBidsFinance_ById(int Id)
        {
            return abstractJobBidsFinanceDao.JobBidsFinance_ById(Id);
        }


        public override bool JobBidsFinance_Delete(int Id)
        {
            return abstractJobBidsFinanceDao.JobBidsFinance_Delete(Id);
        }


        public override SuccessResult<AbstractJobBidsFinance> JobBidsFinance_Upsert(AbstractJobBidsFinance abstractJobBidsFinance)
        {
            return abstractJobBidsFinanceDao.JobBidsFinance_Upsert(abstractJobBidsFinance);
        }
    }
}
