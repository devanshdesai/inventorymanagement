﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class DriverDocumentsServices : AbstractDriverDocumentsServices
    {
        private AbstractDriverDocumentsDao abstractDriverDocumentsDao;

        public DriverDocumentsServices(AbstractDriverDocumentsDao abstractDriverDocumentsDao)
        {
            this.abstractDriverDocumentsDao = abstractDriverDocumentsDao;
        }

        public override SuccessResult<AbstractDriverDocuments> DriverDocuments_Upsert(AbstractDriverDocuments abstractDriverDocuments)
        {
            return abstractDriverDocumentsDao.DriverDocuments_Upsert(abstractDriverDocuments);
        }

        public override SuccessResult<AbstractDriverDocuments> DriverDocuments_ById(int Id)
        {
            return abstractDriverDocumentsDao.DriverDocuments_ById(Id);
        }

        public override PagedList<AbstractDriverDocuments> DriverDocuments_All(PageParam pageParam, int DriverId)
        {
            return abstractDriverDocumentsDao.DriverDocuments_All(pageParam,DriverId);
        }

    }
}
