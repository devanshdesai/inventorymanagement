﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class DocumentTypeServices : AbstractDocumentTypeServices
    {
        private AbstractDocumentTypeDao abstractDocumentTypeDao;

        public DocumentTypeServices(AbstractDocumentTypeDao abstractDocumentTypeDao)
        {
            this.abstractDocumentTypeDao = abstractDocumentTypeDao;
        }

      
        public override PagedList<AbstractDocumentType> DocumentType_All(PageParam pageParam, String Search)
        {
            return this.abstractDocumentTypeDao.DocumentType_All(pageParam, Search);
        }

    }
}
