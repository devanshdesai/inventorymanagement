﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractSupportServices
    {
        public abstract SuccessResult<AbstractSupport> Support_Upsert(AbstractSupport abstractSupport);

        public abstract PagedList<AbstractSupport> Support_All(PageParam pageParam,int CarrierId, int ShipperId, int JobId, int StatusId);

        public abstract SuccessResult<AbstractSupport> Support_Close(int Id);
        public abstract SuccessResult<AbstractSupport> Support_ActInact(int Id);

        public abstract SuccessResult<AbstractSupport> Support_ById(int Id);
    }
}