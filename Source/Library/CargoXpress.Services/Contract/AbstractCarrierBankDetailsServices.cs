﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractCarrierBankDetailsServices
    {
        public abstract SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_Upsert(AbstractCarrierBankDetails abstractCarrierBankDetails);

        public abstract SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_ById(int Id);

        public abstract PagedList<AbstractCarrierBankDetails> CarrierBankDetails_ByCarrierId(PageParam pageParam, string search, int CarrierId);

        public abstract SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_Delete(AbstractCarrierBankDetails abstractCarrierBankDetails);

        public abstract SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_IsDefault(int Id);

    }
}
