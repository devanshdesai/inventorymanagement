﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractJobTruckDriversServices
    {
        public abstract SuccessResult<AbstractJobTruckDrivers> JobTruckDrivers_Upsert(AbstractJobTruckDrivers abstractJobTruckDrivers);

        public abstract SuccessResult<AbstractJobTruckDrivers> JobTruckDrivers_ById(int Id);

        public abstract PagedList<AbstractJobTruckDrivers> JobTruckDrivers_ByJobId(PageParam pageParam, string search, int JobId, int DriverId = 0, int TruckId = 0);

        public abstract bool JobTruckDrivers_Delete(int Id);

    }
}
