﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractJobServices
    {
        public abstract SuccessResult<AbstractJob> Job_ById(int Id);

        public abstract PagedList<AbstractJob> Job_All(PageParam pageParam, string Search, string JobNo, int ShipperId, int CarrierId, int DriverId, int TruckId, int JobStatusId, int TruckTypeId, DateTime? CarrierJobAssignedDate, DateTime? CarrierJobStartDate, DateTime? CarrierJobEndDate, string JobStatus, int PickUpShipperAddressId, int DropOffShipperAddressId, bool IsJobWithIssues, string PickupDate, int TruckServiceId);

        public abstract SuccessResult<AbstractJob> Job_Upsert(AbstractJob abstractJob);

        public abstract SuccessResult<AbstractJob> Job_AssignDriver(AbstractJob abstractJob);

        public abstract SuccessResult<AbstractJob> Job_AssignTruck(AbstractJob abstractJob);

        public abstract SuccessResult<AbstractJob> Job_CarrierJobEndDate(AbstractJob abstractJob);

        public abstract SuccessResult<AbstractJob> Job_CarrierJobStartDate(AbstractJob abstractJob);

        public abstract SuccessResult<AbstractJob> Job_JobStatus(AbstractJob abstractJob);

        public abstract SuccessResult<AbstractJob> Job_AcceptBid(AbstractJob abstractJob);
    }
}
