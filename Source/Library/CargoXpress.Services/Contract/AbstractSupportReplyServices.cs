﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractSupportReplyServices
    {
        public abstract SuccessResult<AbstractSupportReply> SupportReply_Upsert(AbstractSupportReply abstractSupportReply);
        public abstract PagedList<AbstractSupportReply> SupportReply_BySupportId(PageParam pageParam, int SupportId);
    }
}