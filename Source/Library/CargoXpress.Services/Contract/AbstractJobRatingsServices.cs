﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractJobRatingsServices
    {
        public abstract SuccessResult<AbstractJobRatings> JobRatings_Upsert(AbstractJobRatings abstractJobRatings);

        public abstract SuccessResult<AbstractJobRatings> JobRatings_ById(int Id);

        public abstract PagedList<AbstractJobRatings> JobRatings_ByJobId(PageParam pageParam, int JobId, int ShipperId, int CarrierId, int UserType);

    }
}
