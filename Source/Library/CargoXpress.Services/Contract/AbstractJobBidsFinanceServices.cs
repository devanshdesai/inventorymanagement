﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractJobBidsFinanceServices
    {
        public abstract SuccessResult<AbstractJobBidsFinance> JobBidsFinance_ById(int Id);
        public abstract SuccessResult<AbstractJobBidsFinance> JobBidsFinance_Upsert(AbstractJobBidsFinance abstractJobBidsFinance);
        public abstract bool JobBidsFinance_Delete(int Id);
        public abstract PagedList<AbstractJobBidsFinance> JobBidsFinance_All(PageParam pageParam, string search,int JobBidId);
    }
}
