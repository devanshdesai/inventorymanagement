﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractAdminServices
    {
        public abstract SuccessResult<AbstractAdmin> Admin_ById(int Id);

        public abstract SuccessResult<AbstractAdmin> Admin_Login(AbstractAdmin abstractAdmin);

        public abstract bool Admin_ChangePassword(AbstractAdmin abstractAdmin);

        public abstract bool Admin_Logout(int id);

    }
}
