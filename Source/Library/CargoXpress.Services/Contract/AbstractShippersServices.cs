﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractShippersServices
    {
        public abstract SuccessResult<AbstractShippers> Shippers_Upsert(AbstractShippers abstractShippers);
        public abstract SuccessResult<AbstractShippers> Shippers_ById(int Id);
        public abstract PagedList<AbstractShippers> Shippers_All(PageParam pageParam, string search, string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive);
        public abstract SuccessResult<AbstractShippers> Shippers_Login(AbstractShippers abstractShippers);
        public abstract SuccessResult<AbstractShippers> Shippers_ActInact(int Id);
        public abstract SuccessResult<AbstractShippers> Shippers_ChangePassword(AbstractShippers abstractShippers);
        public abstract SuccessResult<AbstractShippers> Shippers_EmailVerified(int Id);
        public abstract SuccessResult<AbstractShippers> Shippers_MobileVerified(int Id);
        public abstract bool Shippers_Logout(int Id);

        public abstract SuccessResult<AbstractShippers> Shippers_ProfileUpdate(AbstractShippers abstractShippers);
    }
}
