﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractCarrierDocuments
    {
        public int Id { get; set; }
        public int CarrierId { get; set; }
        public int DocumentType { get; set; }
        public string Url { get; set; }
        public string DocumentName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("MM/dd/yyyy") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("MM/dd/yyyy") : "-";
        [NotMapped]
        public string UrlStr => Configurations.BaseUrl + Url;
    }
}
