﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractJob
    {
        //public int Id { get; set; }
        //public int ShipperId { get; set; }
        //public int CarrierId { get; set; }
        //public int DriverId { get; set; }
        //public int TruckId { get; set; }
        //public decimal Price { get; set; }
        //public int JobBidId{ get; set; }
        //public string JobNo { get; set; }
        //public int JobStatusId { get; set; }
        //public int TruckTypeId { get; set; }
        //public int TruckServiceId { get; set; }
        //public int SubTruckServiceId { get; set; }
        //public int NoOfContainer { get; set; }
        //public decimal ActualTonage { get; set; }
        //public decimal JobTonage { get; set; }
        //public decimal TotalPallets { get; set; }
        //public decimal ActualWeight { get; set; }
        //public decimal JobWeight { get; set; }
        //public decimal Cbm { get; set; }
        //public string CommodityType { get; set; }
        //public string HsCode { get; set; }
        //public int PickUpShipperAddressId { get; set; }
        //public int DropOffShipperAddressId { get; set; }
        //public string SpecialMessage { get; set; }
        //public string ExpressDate { get; set; }
        //public DateTime ShipperTentativeEndDate { get; set; }
        //public DateTime CarrierJobAssignedDate { get; set; }
        //public DateTime CarrierJobStartDate { get; set; }
        //public DateTime CarrierJobEndDate { get; set; }
        //public bool IsFargile { get; set; }
        //public bool IsExpres { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime UpdatedDate { get; set; }
        //public int UpdatedBy { get; set; }
        //[NotMapped]
        //public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        //[NotMapped]
        //public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

        //public string JobStatus { get; set; }
        //public int TotalBids { get; set; }
        //public decimal LowestBid { get; set; }
        //public decimal HighestBid { get; set; }
        //public string ShipperProfilePhoto { get; set; }
        //[NotMapped]
        //public string ShipperProfilePhotoStr => Configurations.BaseUrl + ShipperProfilePhoto;
        //public string ShipperName { get; set; }
        //public string ShipperEmail { get; set; }
        //public string ShipperPhone { get; set; }
        //public string ShipperCountryCode { get; set; }
        //public string TruckType { get; set; }
        //public string TruckService { get; set; }
        //public string TruckSubService { get; set; }
        //public string PickupDate { get; set; }
        //public decimal Distance { get; set; }
        ////public string PickupShipperAddressLattitude { get; set; }
        ////public string PickupShipperAddressLongitude { get; set; }
        //public string PickUpAddressLongitude { get; set; }
        //public string PickUpAddressLatitude { get; set; }
        //public string PickUpAddress { get; set; }
        ////public string DropoffShipperAddressLattitude { get; set; }
        ////public string DropoffShipperAddressLongitude { get; set; }
        //public string DropOffAddress { get; set; }
        //public string DropOffAddressLongitude { get; set; }
        //public string DropOffAddressLatitude { get; set; }
        //public string CarrierProfilePhoto { get; set; }
        //public string CarrierProfilePhotoStr => Configurations.BaseUrl + CarrierProfilePhoto;

        //public string CarrierName { get; set; }
        //public string CarrierEmail { get; set; }
        //public string CarrierPhone { get; set; }
        //public string CarrierCountryCode { get; set; }
        //public string DriverProfilePhoto { get; set; }
        //public string DriverProfilePhotoStr => Configurations.BaseUrl + DriverProfilePhoto;
        //public string DriverName { get; set; }
        //public string DriverEmail { get; set; }
        //public string DriverPhone { get; set; }
        //public string DriverCountryCode { get; set; }

        //public string TruckPlate { get; set; }
        //public decimal JobAmount { get; set; }
        ////public string CurrentStatus{ get; set; }
        //public DateTime CarrierEstDeliveryDate { get; set; }
        //[NotMapped]
        //public string CarrierEstDeliveryDateStr => CarrierEstDeliveryDate != null ? CarrierEstDeliveryDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        //public string CompanyName { get; set; }        
        //public string DriverLatitude { get; set; }
        //public string DriverLongitude { get; set; }
        //public decimal RatingToCarrier { get; set; }
        //public decimal RatingToShipper { get; set; }
        //public decimal CarrierAvgRating { get; set; }
        //public decimal ShippperAvgRating { get; set; }
        //public string CommentToShipper { get; set; }
        //public string CommentToCarrier { get; set; }
        //public string RatingToShipperDate { get; set; }
        //public string RatingToCarrierDate { get; set; }
        //public decimal NoOfCompletionDays { get; set; }
        //public string TruckLength { get; set; }

        //public string UserTypeName { get; set; }

        //public int ParentId { get; set; }
        //public string ParentJobNo { get; set; }
        public int Id { get; set; }
        public int ShipperId { get; set; }
        public int CarrierId { get; set; }
        public int DriverId { get; set; }
        public int TruckId { get; set; }
        public decimal Price { get; set; }
        public int JobBidId { get; set; }
        public string JobNo { get; set; }
        public int JobStatusId { get; set; }
        public int TruckTypeId { get; set; }
        public int TruckServiceId { get; set; }
        public int SubTruckServiceId { get; set; }
        public int NoOfContainer { get; set; }
        public decimal ActualTonage { get; set; }
        public decimal JobTonage { get; set; }
        public decimal TotalPallets { get; set; }
        public decimal ActualWeight { get; set; }
        public decimal JobWeight { get; set; }
        public decimal Cbm { get; set; }
        public string CommodityType { get; set; }
        public string HsCode { get; set; }
        public int PickUpShipperAddressId { get; set; }
        public int DropOffShipperAddressId { get; set; }
        public string SpecialMessage { get; set; }
        public string ExpressDate { get; set; }
        public string Currency { get; set; }
        public DateTime ShipperTentativeEndDate { get; set; }
        public DateTime CarrierJobAssignedDate { get; set; }
        public DateTime CarrierJobStartDate { get; set; }
        public DateTime CarrierJobEndDate { get; set; }
        public bool IsFargile { get; set; }
        public bool IsExpres { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string CarrierJobStartDateStr => CarrierJobStartDate != null ? CarrierJobStartDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string CarrierJobEndDateStr => CarrierJobEndDate != null ? CarrierJobEndDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        public string JobStatus { get; set; }
        public int TotalBids { get; set; }
        public decimal LowestBid { get; set; }
        public decimal HighestBid { get; set; }
        public string ShipperProfilePhoto { get; set; }
        [NotMapped]
        public string ShipperProfilePhotoStr => Configurations.BaseUrl + ShipperProfilePhoto;
        public string ShipperName { get; set; }
        public string ShipperEmail { get; set; }
        public string ShipperPhone { get; set; }
        public string ShipperCountryCode { get; set; }
        public string TruckType { get; set; }
        public string TruckService { get; set; }
        public string TruckSubService { get; set; }
        public string PickupDate { get; set; }
        public decimal Distance { get; set; }
        //public string PickupShipperAddressLattitude { get; set; }
        //public string PickupShipperAddressLongitude { get; set; }
        public string PickUpAddressLongitude { get; set; }
        public string PickUpAddressLatitude { get; set; }
        public string PickUpAddress { get; set; }
        //public string DropoffShipperAddressLattitude { get; set; }
        //public string DropoffShipperAddressLongitude { get; set; }
        public string DropOffAddress { get; set; }
        public string DropOffAddressLongitude { get; set; }
        public string DropOffAddressLatitude { get; set; }
        public string CarrierProfilePhoto { get; set; }
        public string CarrierProfilePhotoStr => Configurations.BaseUrl + CarrierProfilePhoto;

        public string CarrierName { get; set; }
        public string CarrierEmail { get; set; }
        public string CarrierPhone { get; set; }
        public string CarrierCountryCode { get; set; }
        public string DriverProfilePhoto { get; set; }
        public string DriverProfilePhotoStr => Configurations.BaseUrl + DriverProfilePhoto;
        public string DriverName { get; set; }
        public string DriverEmail { get; set; }
        public string DriverPhone { get; set; }
        public string DriverCountryCode { get; set; }

        public string TruckPlate { get; set; }
        public decimal JobAmount { get; set; }
        //public string CurrentStatus{ get; set; }
        public DateTime CarrierEstDeliveryDate { get; set; }
        [NotMapped]
        public string CarrierEstDeliveryDateStr => CarrierEstDeliveryDate != null ? CarrierEstDeliveryDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        public string CompanyName { get; set; }
        public string DriverLatitude { get; set; }
        public string DriverLongitude { get; set; }
        public decimal RatingToCarrier { get; set; }
        public decimal RatingToShipper { get; set; }
        public decimal CarrierAvgRating { get; set; }
        public decimal ShippperAvgRating { get; set; }
        public string CommentToShipper { get; set; }
        public string CommentToCarrier { get; set; }
        public string RatingToShipperDate { get; set; }
        public string RatingToCarrierDate { get; set; }
        public decimal NoOfCompletionDays { get; set; }
        public string TruckLength { get; set; }

        public string UserTypeName { get; set; }

        public int ParentId { get; set; }
        public string ParentJobNo { get; set; }
        public bool IsJobWithIssues { get; set; }

        public bool IsSamePath { get; set; }

        public string CubicMeter { get; set; }
        public string Liters { get; set; }
        public string Cars { get; set; }
        public string DeliveryItem { get; set; }
        public bool IsCrossBorder { get; set; }
        public decimal Kg { get; set; }
        public string PaymentType { get; set; }
        public string BreakDown { get; set; }
        public bool IsFinance { get; set; }
        public decimal Percentage { get; set; }

    }
}
