﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractJobDocuments
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public int UserType { get; set; }
        public int DocumentTypeId { get; set; }
        public int Status { get; set; }
        public string Url { get; set; }//Add by Krinal
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UrlStr => Configurations.BaseUrl + Url;

    }
}
