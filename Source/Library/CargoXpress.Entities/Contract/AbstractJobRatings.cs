﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractJobRatings
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public string RatingToShipper { get; set; }
        public string CommentToShipper { get; set; }
        public DateTime RatingToShipperDate { get; set; }
        public string RatingToCarrier { get; set; }
        public string CommentToCarrier { get; set; }
        public DateTime RatingToCarrierDate { get; set; }
        public bool IsActive { get; set; }
        public int Type { get; set; }
        public int ShipperId { get; set; }
        public int CarrierId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public int UserType { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("mm/dd/yyyy") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("mm/dd/yyyy") : "-";
        [NotMapped]
        public string RatingToShipperDateStr => RatingToShipperDate != null ? RatingToShipperDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}
