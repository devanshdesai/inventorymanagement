﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractTruckPrefferedDestinations
    {
        public int Id { get; set; }
        public int TruckId { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string City { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }       
        public string Country { get; set; }
        public string State { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("MM/dd/yyyy") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("MM/dd/yyyy") : "-";
        [NotMapped]
        public string Destination
        {
            get
            {
                string destination = "";
                if (!string.IsNullOrEmpty(City))
                    destination += City;
                if (!string.IsNullOrEmpty(State))
                    destination += ", " + State;
                if (!string.IsNullOrEmpty(Country))
                    destination += ", " + Country;

                return destination;
            }
        }
    }
}
