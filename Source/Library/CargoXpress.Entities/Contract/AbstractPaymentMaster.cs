﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractPaymentMaster
    {
        //public int Id { get; set; }
        //public int JobId { get; set; }
        //public int PaidByUserType { get; set; }
        //public decimal Amount { get; set; }
        //public int PaidToUserType { get; set; }
        //public string Type { get; set; }
        //public string Note { get; set; }
        //public string PaymentDate { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime UpdatedDate { get; set; }
        //public int UpdatedBy { get; set; }
        //public string JobNumber { get; set; }
        //public string PaymentFromUserType { get; set; }
        //public string PaymentToUserType { get; set; }

        //[NotMapped]
        //public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        //[NotMapped]
        //public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        public int Id { get; set; }
        public int JobId { get; set; }
        public int PaidByUserType { get; set; }
        public decimal Amount { get; set; }
        public int PaidToUserType { get; set; }
        public string Type { get; set; }
        public string Note { get; set; }
        public string PaymentDate { get; set; }
        public string Url { get; set; }
        public string Currency { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public string JobNumber { get; set; }
        public string PaymentFromUserType { get; set; }
        public string PaymentToUserType { get; set; }

        public bool IsRecieved { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public bool IsPaymentExists { get; set; }
        public string JobNo { get; set; }
        public string ShipperName { get; set; }
        public string ShipperEmail { get; set; }
        public string ShipperPhoneNumber { get; set; }
        public string CarrierPhoneNumber { get; set; }
        public string CarrierName { get; set; }
        public string CarrierEmail { get; set; }
        public string PaidByUserTypeName { get; set; }
        public string PaidToUserTypeName { get; set; }
        public decimal AmountPending { get; set; }
        public decimal Price { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

        public string UrlStr => Configurations.BaseUrl + Url;


    }
}
