﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractJobBids
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public int CarrierId { get; set; }
        public decimal Price { get; set; }
        public decimal JobCompletionDays { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }

        public int JobBidStatus { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

        public string CarrierName { get; set; }
        public string CarrierPhoneNumber { get; set; }
        public string CarrierEmail { get; set; }
        public string CompanyName { get; set; }
        public string CarrierFileURL { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        [NotMapped]
        public string CarrierFileURLStr => Configurations.BaseUrl + CarrierFileURL;
        public string RatingToCarrier { get; set; }
        public string JobNo { get; set; }
        public string CommodityType { get; set; }
        public DateTime JobCreatedDate { get; set; }
        public string JobCreatedDateStr => JobCreatedDate != null ? JobCreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        public bool IsAccepted { get; set; }

        public int ShipperId { get; set; }
        public int DriverId { get; set; }
        public int TruckId { get; set; }
        public int TruckServiceId { get; set; }
        public string ActualTonage { get; set; }
        public int JobStatusId { get; set; }
        public int TruckTypeId { get; set; }
        public DateTime CarrierJobAssignedDate { get; set; }
        public DateTime CarrierJobStartDate { get; set; }
        public DateTime CarrierJobEndDate { get; set; }
        public string PickUpAddress { get; set; }
        public string PickUpAddressLongitude { get; set; }
        public string PickUpAddressLatitude { get; set; }
        public string DropOffAddress { get; set; }
        public string DropOffAddressLongitude { get; set; }
        public string DropOffAddressLatitude { get; set; }
        public string ExpressDate { get; set; }
        public string SpecialMessage { get; set; }
        public string PickUpDate { get; set; }
        public decimal Distance { get; set; }
        public string TruckLength { get; set; }
        public int PickUpShipperAddressId { get; set; }
        public int DropOffShipperAddressId { get; set; }
        public string JobStatus { get; set; }
        public string PaymentType { get; set; }
        public string BreakDown { get; set; }
        public bool IsFinance { get; set; }
        public decimal Percentage { get; set; }

    }
}
