﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractJobDriverLocation
    { 
        public int Id { get; set; }
        public int JobId { get; set; }
        public int DriverId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CapturedDate { get; set; }
        public DateTime CreatedDate { get; set; }

        public string JobNo { get; set; }
        public string DriverDetails { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("mm/dd/yyyy") : "-";
        
    }
}
