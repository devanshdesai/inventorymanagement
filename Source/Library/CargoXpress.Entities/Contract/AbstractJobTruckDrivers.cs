﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractJobTruckDrivers
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public int TruckId { get; set; }
        public int DriverId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public string DriverFirstName { get; set; }
        public string DriverLastName { get; set; }
        public string DriverEamil { get; set; }
        public string DriverPhoneNumber { get; set; }
        public string TruckPlateNumber { get; set; }
        public string TruckName { get; set; }
        public string DriverProfile { get; set; }
        public int DriverCountryCode { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string FileUrlStr => Configurations.BaseUrl + DriverProfile;

    }
}
