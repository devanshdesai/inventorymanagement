﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class ShipperAddressesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShipperAddressesService abstractShipperAddressesService;

        #endregion

        #region Cnstr
        public ShipperAddressesV1Controller(AbstractShipperAddressesService abstractShipperAddressesService)
        {
            this.abstractShipperAddressesService = abstractShipperAddressesService;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperAddresses_Upsert")]
        public async Task<IHttpActionResult> ShipperAddresses_Upsert(ShipperAddresses shipperAddresses)
        {
            var quote = abstractShipperAddressesService.ShipperAddresses_Upsert(shipperAddresses);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperAddresses_ById")]
        public async Task<IHttpActionResult> ShipperAddresses_ById(int Id)
        {
            var quote = abstractShipperAddressesService.ShipperAddresses_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperAddresses_All")]
        public async Task<IHttpActionResult> ShipperAddresses_All(PageParam pageParam, int ShipperId)
        {
            var quote = abstractShipperAddressesService.ShipperAddresses_All(pageParam,ShipperId);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
