﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class TruckPrefferedDestinationsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTruckPrefferedDestinationsServices abstractTruckPrefferedDestinationsServices;

        #endregion

        #region Cnstr
        public TruckPrefferedDestinationsV1Controller(AbstractTruckPrefferedDestinationsServices abstractTruckPrefferedDestinationsServices)
        {
            this.abstractTruckPrefferedDestinationsServices = abstractTruckPrefferedDestinationsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckPrefferedDestinations_Upsert")]
        public async Task<IHttpActionResult> TruckPrefferedDestinations_Upsert(TruckPrefferedDestinations truckPreferedDestinations)
        {
            var quote = abstractTruckPrefferedDestinationsServices.TruckPrefferedDestinations_Upsert(truckPreferedDestinations);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckPrefferedDestinations_ById")]
        public async Task<IHttpActionResult> TruckPrefferedDestinations_ById(int Id)
        {
            var quote = abstractTruckPrefferedDestinationsServices.TruckPrefferedDestinations_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckPrefferedDestinations_All")]
        public async Task<IHttpActionResult> TruckPrefferedDestinations_All(PageParam pageParam,int TruckId = 0)
        {
            var quote = abstractTruckPrefferedDestinationsServices.TruckPrefferedDestinations_All(pageParam,TruckId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
