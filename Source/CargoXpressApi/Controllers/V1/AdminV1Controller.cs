﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class AdminV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAdminServices  abstractAdminServices;
        private readonly AbstractJobServices abstractJobServices;
        private readonly AbstractCarriersServices abstractCarriersServices;
        private readonly AbstractShippersServices abstractShippersServices;
        private readonly AbstractDriversService abstractDriversService;
        #endregion

        #region Cnstr
        public AdminV1Controller(AbstractAdminServices abstractAdminServices, AbstractJobServices abstractJobServices,
            AbstractCarriersServices abstractCarriersServices, AbstractShippersServices abstractShippersServices,
            AbstractDriversService abstractDriversService)
        {
            this.abstractAdminServices = abstractAdminServices;
            this.abstractShippersServices = abstractShippersServices;
            this.abstractCarriersServices = abstractCarriersServices;
            this.abstractJobServices = abstractJobServices;
            this.abstractDriversService = abstractDriversService;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_Login")]
        public async Task<IHttpActionResult> Admin_Login(Admin admin)
        {
            var quote = abstractAdminServices.Admin_Login(admin);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_Logout")]
        public async Task<IHttpActionResult> Admin_Logout(int Id)
        {
            var quote = abstractAdminServices.Admin_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ById")]
        public async Task<IHttpActionResult> Admin_ById(int Id)
        {
            var quote = abstractAdminServices.Admin_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ChangePassword")]
        public async Task<IHttpActionResult> Admin_ChangePassword(Admin admin)
        {
            var quote = abstractAdminServices.Admin_ChangePassword(admin);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("SampleDeviceToken")]
        public async Task<IHttpActionResult> SampleDeviceToken(string deviceToken, string body)
        {
            EmailHelper.SendPushNotification(deviceToken, body, "test");
            return this.Content((HttpStatusCode)200, "");
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("DriverSampleDeviceToken")]
        public async Task<IHttpActionResult> DriverSampleDeviceToken(string deviceToken, string body)
        {
            EmailHelper.DriverSendPushNotification(deviceToken, body, "test");
            return this.Content((HttpStatusCode)200, "");
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("SendSms")]
        public async Task<IHttpActionResult> SendSms(string countrymobile)
        {
            string otp = CommonHelper.GenerateRandomNo();
            string message = "Aurora Tech Africa - Your One Time Password is " + otp;
            string status = EmailHelper.SendMsgViaTwilio(message, countrymobile);
            if(status == "SUCCESS")
            {
                return this.Content((HttpStatusCode)200, otp);
            }
            else
            {
                return this.Content((HttpStatusCode)400, status);
            }
            
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("NotCapturing")]
        public async Task<IHttpActionResult> NotCapturing(int Id, int DriverId)
        {
            var quote = abstractJobServices.Job_ById(Id);

            var driver = abstractDriversService.Drivers_ById(DriverId);
            string driverNumber = driver.Item.CountryCode + "" + driver.Item.PhoneNumber;
            driverNumber = driverNumber.Remove(2, 1);

            string title = "ALERT - Auorara";
            string body = "The driver's (" + driver.Item.FirstName + " " + driver.Item.LastName + ") device for the Job (" + quote.Item.JobNo + ") has stopped capturing the location - Call on +"+ driverNumber + " to let them know";

            var shipper = abstractShippersServices.Shippers_ById(quote.Item.ShipperId);

            var carrier = abstractCarriersServices.Carriers_ById(quote.Item.CarrierId);

            EmailHelper.SendPushNotification(shipper.Item.DeviceToken, body, title, shipper.Item.CountryCode + "" + shipper.Item.PhoneNumber);
            EmailHelper.SendPushNotification(carrier.Item.DeviceToken, body, title, carrier.Item.CountryCode + "" + carrier.Item.PhoneNumber);

            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
