﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;
using System.Data;
using System.Text;

namespace CargoXpressApi.Controllers.V1
{
    public class RouteMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractRouteMasterServices abstractRouteMasterServices;

        #endregion

        #region Cnstr
        public RouteMasterV1Controller(AbstractRouteMasterServices abstractRouteMastersServices)
        {
            this.abstractRouteMasterServices = abstractRouteMastersServices;
        }
        #endregion

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteMaster_Upsert")]
        public async Task<IHttpActionResult> RouteMaster_Upsert(RouteMaster routeMaster)
        {
            DataTable dt = GetLatLong(routeMaster.PickUpLocationName);
            if (dt.Rows.Count > 0)
            {
                routeMaster.PickUpLat = Convert.ToString(dt.Rows[0]["Latitude"]);
                routeMaster.PickUpLong = Convert.ToString(dt.Rows[0]["Longitude"]);
            }
            
            dt = GetLatLong(routeMaster.DropOffLocationName);
            if (dt.Rows.Count > 0)
            {
                routeMaster.DropOffLat = Convert.ToString(dt.Rows[0]["Latitude"]);
                routeMaster.DropOffLong = Convert.ToString(dt.Rows[0]["Longitude"]);
            }
            var quote = abstractRouteMasterServices.RouteMaster_Upsert(routeMaster);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteMaster_ById")]
        public async Task<IHttpActionResult> RouteMaster_ById(int Id)
        {
            var quote = abstractRouteMasterServices.RouteMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteMaster_ByJobId")]
        public async Task<IHttpActionResult> RouteMaster_ByJobId(PageParam pageParam,string search="", int JobId=0,int JobTruckDriverId=0)
        {
            var quote = abstractRouteMasterServices.RouteMaster_ByJobId(pageParam,search, JobId, JobTruckDriverId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteMaster_Delete")]
        public async Task<IHttpActionResult> RouteMaster_Delete(int Id)
        {
            var quote = abstractRouteMasterServices.RouteMaster_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        private DataTable GetLatLong(string address)
        {
            //string url = "https://maps.google.com/maps/api/geocode/xml?address=" + address + "&key=AIzaSyABofRnRkpT_4m344ZOsyRnT90uSGF5H6A";
            string url = "https://maps.google.com/maps/api/geocode/xml?address=" + address + "&key=AIzaSyABofRnRkpT_4m344ZOsyRnT90uSGF5H6A";
            WebRequest request = WebRequest.Create(url);

            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                DataTable dtCoordinates = new DataTable();
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    DataSet dsResult = new DataSet();
                    dsResult.ReadXml(reader);
                    dtCoordinates.Columns.AddRange(new DataColumn[4] { new DataColumn("Id", typeof(int)),
                    new DataColumn("Address", typeof(string)),
                    new DataColumn("Latitude",typeof(string)),
                    new DataColumn("Longitude",typeof(string)) });
                    foreach (DataRow row in dsResult.Tables["result"].Rows)
                    {
                        string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
                        DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
                        dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);
                    }
                }
                return dtCoordinates;
            }
        }





    }
}
