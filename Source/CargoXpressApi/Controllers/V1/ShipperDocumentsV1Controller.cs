﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;

namespace CargoXpressApi.Controllers.V1
{
    public class ShipperDocumentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShiperDocumentsServices abstractShiperDocumentsServices;

        #endregion

        #region Cnstr
        public ShipperDocumentsV1Controller(AbstractShiperDocumentsServices abstractShiperDocumentsServices)
        {
            this.abstractShiperDocumentsServices = abstractShiperDocumentsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperDocuments_Upsert")]
        public async Task<IHttpActionResult> ShipperDocuments_Upsert()
        {
            ShiperDocuments shiperDocuments = new ShiperDocuments(); 
            var httpRequest = HttpContext.Current.Request;
            shiperDocuments.ShipperId = Convert.ToInt32(httpRequest.Params["ShipperId"]);
            shiperDocuments.DocumentType = Convert.ToInt32(httpRequest.Params["DocumentType"]);
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "ShipperDocuments/" + shiperDocuments.ShipperId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                shiperDocuments.Url = basePath + fileName;
            }

            var quote = abstractShiperDocumentsServices.ShipperDocuments_Upsert(shiperDocuments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperDocuments_ById")]
        public async Task<IHttpActionResult> ShipperDocuments_ById(int Id)
        {
            var quote = abstractShiperDocumentsServices.ShiperDocuments_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperDocuments_All")]
        public async Task<IHttpActionResult> ShipperDocuments_All(PageParam pageParam, int ShipperId)
        {
            var quote = abstractShiperDocumentsServices.ShiperDocuments_All(pageParam,ShipperId);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
