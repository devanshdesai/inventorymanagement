﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;

namespace CargoXpressApi.Controllers.V1
{
    public class DriverDocumentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDriverDocumentsServices  abstractDriverDocumentsServices;

        #endregion

        #region Cnstr
        public DriverDocumentsV1Controller(AbstractDriverDocumentsServices abstractDriverDocumentsServices)
        {
            this.abstractDriverDocumentsServices = abstractDriverDocumentsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("DriverDocuments_Upsert")]
        public async Task<IHttpActionResult> DriverDocuments_Upsert()
        {
            DriverDocuments driverDocuments = new DriverDocuments();
            var httpRequest = HttpContext.Current.Request;
            driverDocuments.DriverId = Convert.ToInt32(httpRequest.Params["DriverId"]);
            driverDocuments.DocumentType = Convert.ToInt32(httpRequest.Params["DocumentType"]);
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "DriverDocuments/" + driverDocuments.DriverId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                driverDocuments.Url = basePath + fileName;
            }

            var quote = abstractDriverDocumentsServices.DriverDocuments_Upsert(driverDocuments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("DriverDocuments_ById")]
        public async Task<IHttpActionResult> DriverDocuments_ById(int Id)
        {
            var quote = abstractDriverDocumentsServices.DriverDocuments_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("DriverDocuments_All")]
        public async Task<IHttpActionResult> DriverDocuments_All(PageParam pageParam,int DriverId = 0)
        {
            var quote = abstractDriverDocumentsServices.DriverDocuments_All(pageParam,DriverId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
