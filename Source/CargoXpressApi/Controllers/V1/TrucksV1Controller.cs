﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class TrucksV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTrucksService abstractTrucksServices;

        #endregion

        #region Cnstr
        public TrucksV1Controller(AbstractTrucksService abstractTrucksServices)
        {
            this.abstractTrucksServices = abstractTrucksServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("Trucks_Upsert")]
        public async Task<IHttpActionResult> Trucks_Upsert(Trucks trucks)
        {
            var quote = abstractTrucksServices.Trucks_Upsert(trucks);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Trucks_ById")]
        public async Task<IHttpActionResult> Trucks_ById(int Id)
        {
            var quote = abstractTrucksServices.Trucks_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Trucks_All")]
        public async Task<IHttpActionResult> Trucks_All(PageParam pageParam, string search="", string PlateNumber="", string CompanyName="", int TruckTypeId =0,int LoadCapacity =0 , int CarrierId=0)
        {
            var quote = abstractTrucksServices.Trucks_All(pageParam,search, PlateNumber, CompanyName, TruckTypeId, LoadCapacity, CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Trucks_ActInact")]
        public async Task<IHttpActionResult> Trucks_ActInact(int Id)
        {
            var quote = abstractTrucksServices.Trucks_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Trucks_JobActInact")]
        public async Task<IHttpActionResult> Trucks_JobActInact(int Id)
        {
            var quote = abstractTrucksServices.Trucks_JobActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
