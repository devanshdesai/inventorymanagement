﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace CargoXpressApi.Controllers.V1
{
    public class ShipperV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShippersServices abstractShippersServices;
        private readonly AbstractCarriersServices abstractCarriersServices;

        #endregion

        #region Cnstr
        public ShipperV1Controller(AbstractShippersServices abstractShippersServices, AbstractCarriersServices abstractCarriersServices)
        {
            this.abstractShippersServices = abstractShippersServices;
            this.abstractCarriersServices = abstractCarriersServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperLogin")]
        public async Task<IHttpActionResult> ShipperLogin(Shippers shippers)
        {
            var quote = abstractShippersServices.Shippers_Login(shippers);
            if (shippers.LoginType == false && quote.Code == 200 && quote.Item != null)
            {
                try
                {
                    shippers.Otp = CommonHelper.GenerateRandomNo();
                    //EmailHelper.SendMsgViaTwilio(cleaners.Otp, quote.Item.Mobile);
                }
                catch (Exception ex)
                {

                }
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Shippers_Logout")]
        public async Task<IHttpActionResult> Shippers_Logout(int Id)
        {
            var quote = abstractShippersServices.Shippers_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperUpsert")]
        public async Task<IHttpActionResult> Shippers_Upsert(Shippers Shippers)
        {
            var quote = abstractShippersServices.Shippers_Upsert(Shippers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Shippers_ById")]
        public async Task<IHttpActionResult> Shipper_ById(int Id)
        {
            var quote = abstractShippersServices.Shippers_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Shippers_All")]
        public async Task<IHttpActionResult> Shippers_All(PageParam pageParam,string search="", string Name="", string Email="", int CountryId =0, string PhoneNumber ="", bool? IsActive = null)
        {
            var quote = abstractShippersServices.Shippers_All(pageParam,search, Name, Email, CountryId, PhoneNumber,IsActive);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Shippers_ActInact")]
        public async Task<IHttpActionResult> Shippers_ActInact(int Id)
        {
            var quote = abstractShippersServices.Shippers_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Shippers_EmailVerify")]
        public async Task<IHttpActionResult> Shippers_EmailVerify(int Id)
        {
            var quote = abstractShippersServices.Shippers_EmailVerified(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Shippers_MobileVerify")]
        public async Task<IHttpActionResult> Shippers_MobileVerify(int Id)
        {
            var quote = abstractShippersServices.Shippers_MobileVerified(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Shippers_ChangePassword")]
        public async Task<IHttpActionResult> Shippers_ChangePassword(Shippers Shippers)
        {
            var quote = abstractShippersServices.Shippers_ChangePassword(Shippers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Shippers_ProfileUpdate")]
        public async Task<IHttpActionResult> Shippers_ProfileUpdate()
        {
            Shippers shippers = new Shippers();
            var httpRequest = HttpContext.Current.Request;
            shippers.Id = Convert.ToInt32(httpRequest.Params["Id"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "ShipperProfile/" + shippers.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                shippers.FileURL = basePath + fileName;
            }
            var quote = abstractShippersServices.Shippers_ProfileUpdate(shippers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Shipper_GetOtp")]
        public async Task<IHttpActionResult> Shipper_GetOtp(string mobileNo)
        {
            try
            {
                string Otp = CommonHelper.GenerateRandomNo();
                //EmailHelper.SendMsgViaTwilio(Otp, mobileNo);
                return this.Content((HttpStatusCode)200, Otp);
            }
            catch (Exception ex)
            {
                return this.Content((HttpStatusCode)500, "An error occured in the twilio");
            }

        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Shipper_VerifyMobileNoForJob")]
        public async Task<IHttpActionResult> Shipper_VerifyMobileNoForJob(int CarrierId, string ShipperMobileNo, int CountryId,string CountryCode)
        {
            try
            {
                string var_sql = "select Id from Shippers where PhoneNumber='"+ ShipperMobileNo + "' and CountryId=" + CountryId;
                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                sda.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    int ShipperId = Convert.ToInt32(dt.Rows[0]["Id"]);
                    var result = abstractShippersServices.Shippers_ById(ShipperId);
                    if(result.Item != null)
                    {
                        string Otp = CommonHelper.GenerateRandomNo();
                        var carrier = abstractCarriersServices.Carriers_ById(CarrierId);
                        string message = "Greetings from Aurora Tech Africa - Hello " + result.Item.FirstName + " " + result.Item.LastName + ", " + carrier.Item.FirstName + " " + carrier.Item.LastName + " (" + carrier.Item.PhoneNumberStr + " - " + carrier.Item.Email + ") is trying to create a job for you. If you are aware about the same kindly forward the OTP : " + Otp + " to them. If not then no action is required";
                        EmailHelper.SendMsgViaTwilio(message, CountryCode+result.Item.PhoneNumber);
                        result.Item.Otp = Otp;
                        return this.Content((HttpStatusCode)200, result);
                    }
                    else
                    {
                        return this.Content((HttpStatusCode)404, "Phone number is incorrect !");
                    }
                }
                else
                {
                    return this.Content((HttpStatusCode)404, "Phone number is incorrect !");
                }
            }
            catch (Exception ex)
            {
                return this.Content((HttpStatusCode)500, "An error occured");
            }

        }

    }
}
