﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class JobBidsFinanceV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobBidsFinanceServices  abstractJobBidsFinanceServices;

        #endregion

        #region Cnstr
        public JobBidsFinanceV1Controller(AbstractJobBidsFinanceServices abstractJobBidsFinanceServices)
        {
            this.abstractJobBidsFinanceServices = abstractJobBidsFinanceServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBidsFinance_Upsert")]
        public async Task<IHttpActionResult> JobBidsFinance_Upsert(JobBidsFinance JobBidsFinance)
        {
            var quote = abstractJobBidsFinanceServices.JobBidsFinance_Upsert(JobBidsFinance);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBidsFinance_ById")]
        public async Task<IHttpActionResult> JobBidsFinance_ById(int Id)
        {
            var quote = abstractJobBidsFinanceServices.JobBidsFinance_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBidsFinance_All")]
        public async Task<IHttpActionResult> JobBidsFinance_All(PageParam pageParam, string search = "", int JobBidId = 0)
        {
            var quote = abstractJobBidsFinanceServices.JobBidsFinance_All(pageParam, search, JobBidId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBidsFinance_Delete")]
        public async Task<IHttpActionResult> JobBidsFinance_Delete(int Id)
        {
            var quote = abstractJobBidsFinanceServices.JobBidsFinance_Delete(Id);
            return this.Content((HttpStatusCode)200, "Deleted Successfully");
        }
    }
}
