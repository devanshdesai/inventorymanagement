﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class JobStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobStatusServices  abstractJobStatusServices;

        #endregion

        #region Cnstr
        public JobStatusV1Controller(AbstractJobStatusServices abstractJobStatusServices)
        {
            this.abstractJobStatusServices = abstractJobStatusServices;
        }
        #endregion

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobStatus_All")]
        public async Task<IHttpActionResult> JobStatus_All(PageParam pageParam, String Search = "")
        {
            var quote = abstractJobStatusServices.JobStatus_All(pageParam,Search);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
