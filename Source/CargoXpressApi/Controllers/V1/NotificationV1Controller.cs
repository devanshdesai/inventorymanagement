﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class NotificationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractNotificationServices  abstractNotificationServices;

        #endregion

        #region Cnstr
        public NotificationV1Controller(AbstractNotificationServices abstractNotificationServices)
        {
            this.abstractNotificationServices = abstractNotificationServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Notification_Upsert")]
        public async Task<IHttpActionResult> Notification_Upsert(Notification carrierBankDetails)
        {
            var quote = abstractNotificationServices.Notification_Upsert(carrierBankDetails);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Notification_Count")]
        public async Task<IHttpActionResult> Notification_Count(Notification abstractNotification)
        {
            var quote = abstractNotificationServices.Notification_Count(abstractNotification);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        

        [System.Web.Http.HttpPost]
        [InheritedRoute("Notification_IsReadUpdate")]
        public async Task<IHttpActionResult> Notification_IsReadUpdate(Notification abstractNotification)
        {
            var quote = abstractNotificationServices.Notification_IsReadUpdate(abstractNotification);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Notification_All")]
        public async Task<IHttpActionResult> Notification_All(PageParam pageParam,string search="", int UserId = 0, int UserType = 0)
        {
            var quote = abstractNotificationServices.Notification_All(pageParam,search, UserId, UserType);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
