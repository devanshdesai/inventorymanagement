﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class CarrierPreferedDestinationsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCarrierPreferedDestinationsServices  abstractCarrierPreferedDestinationsServices;

        #endregion

        #region Cnstr
        public CarrierPreferedDestinationsV1Controller(AbstractCarrierPreferedDestinationsServices abstractCarrierPreferedDestinationsServices)
        {
            this.abstractCarrierPreferedDestinationsServices = abstractCarrierPreferedDestinationsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierPreferedDestinations_Upsert")]
        public async Task<IHttpActionResult> CarrierPreferedDestinations_Upsert(CarrierPreferedDestinations carriers)
        {
            var quote = abstractCarrierPreferedDestinationsServices.CarrierPreferedDestinations_Upsert(carriers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierPreferedDestinations_ById")]
        public async Task<IHttpActionResult> CarrierPreferedDestinations_ById(int Id)
        {
            var quote = abstractCarrierPreferedDestinationsServices.CarrierPreferedDestinations_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierPreferedDestinations_All")]
        public async Task<IHttpActionResult> CarrierPreferedDestinations_All(PageParam pageParam,int CarrierId = 0)
        {
            var quote = abstractCarrierPreferedDestinationsServices.CarrierPreferedDestinations_All(pageParam,CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierPreferedDestinations_Delete")]
        public async Task<IHttpActionResult> CarrierPreferedDestinations_Delete(int CarrierId)
        {
            var quote = abstractCarrierPreferedDestinationsServices.CarrierPreferedDestinations_Delete(CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
