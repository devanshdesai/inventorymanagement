﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;

namespace CargoXpressApi.Controllers.V1
{
    public class PaymentMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPaymentMasterServices abstractPaymentMasterServices;

        #endregion

        #region Cnstr
        public PaymentMasterV1Controller(AbstractPaymentMasterServices abstractPaymentMasterServices)
        {
            this.abstractPaymentMasterServices = abstractPaymentMasterServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("TransactionReport")]
        public async Task<IHttpActionResult> TransactionReport(PageParam pageParam, string search = "", int JobId = 0, int PaidByUserType = 0, int PaidToUserType = 0, int ShipperId = 0, int CarrierId = 0, string FromDate = null, string ToDate = null)
        {
            var quote = abstractPaymentMasterServices.TransactionReport(pageParam, search, JobId, PaidByUserType, PaidToUserType, ShipperId, CarrierId, FromDate, ToDate);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("PaymentMaster_Upsert")]
        public async Task<IHttpActionResult> PaymentMaster_Upsert()
        {
            var httpRequest = HttpContext.Current.Request;
            var paymentMaster = new PaymentMaster();
            paymentMaster.JobId = Convert.ToInt32(httpRequest.Params["JobId"]);
            paymentMaster.PaidByUserType = Convert.ToInt32(httpRequest.Params["PaidByUserType"]);
            paymentMaster.Amount = Convert.ToDecimal(httpRequest.Params["Amount"]);
            paymentMaster.Type = Convert.ToString(httpRequest.Params["Type"]);
            paymentMaster.PaidToUserType = Convert.ToInt32(httpRequest.Params["PaidToUserType"]);
            paymentMaster.Note = Convert.ToString(httpRequest.Params["Note"]);
            paymentMaster.PaymentDate = Convert.ToString(httpRequest.Params["PaymentDate"]);
            paymentMaster.Url = Convert.ToString(httpRequest.Params["Url"]);



            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "PaymentReceipt/" + paymentMaster.JobId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                paymentMaster.Url = basePath + fileName;
            }

            var quote = abstractPaymentMasterServices.PaymentMaster_Upsert(paymentMaster);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("PaymentMaster_ById")]
        public async Task<IHttpActionResult> PaymentMaster_ById(int Id)
        {
            var quote = abstractPaymentMasterServices.PaymentMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("PaymentMaster_All")]
        public async Task<IHttpActionResult> PaymentMaster_All(PageParam pageParam, string Search = "", int JobId = 0, int PaidByUserType = 0, int PaidToUserType = 0)
        {
            var quote = abstractPaymentMasterServices.PaymentMaster_All(pageParam, Search, JobId, PaidByUserType, PaidToUserType);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("PaymentMaster_Delete")]
        public async Task<IHttpActionResult> PaymentMaster_Delete(int Id)
        {
            var quote = abstractPaymentMasterServices.PaymentMaster_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("PaymentMaster_RecievedNotRecieved")]
        public async Task<IHttpActionResult> PaymentMaster_RecievedNotRecieved(int Id)
        {
            var quote = abstractPaymentMasterServices.PaymentMaster_RecievedNotRecieved(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TransactionReport_ADVANCED")]
        public async Task<IHttpActionResult> TransactionReport_ADVANCED(PageParam pageParam, string search = "", int ShipperId = 0, int CarrierId = 0, string FromDate = null, string ToDate = null)
        {
            var quote = abstractPaymentMasterServices.TransactionReport_ADVANCED(pageParam, search, ShipperId, CarrierId, FromDate, ToDate);
            return this.Content((HttpStatusCode)200, quote);
        }

    }

    }

