﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;
using System.Data;
using System.Text;

namespace CargoXpressApi.Controllers.V1
{
    public class JobTruckDriversV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobTruckDriversServices abstractJobTruckDriversServices;

        #endregion

        #region Cnstr
        public JobTruckDriversV1Controller(AbstractJobTruckDriversServices abstractJobTruckDriverssServices)
        {
            this.abstractJobTruckDriversServices = abstractJobTruckDriverssServices;
        }
        #endregion

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobTruckDrivers_Upsert")]
        public async Task<IHttpActionResult> JobTruckDrivers_Upsert(JobTruckDrivers jobTruckDrivers)
        {
            var quote = abstractJobTruckDriversServices.JobTruckDrivers_Upsert(jobTruckDrivers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobTruckDrivers_ById")]
        public async Task<IHttpActionResult> JobTruckDrivers_ById(int Id)
        {
            var quote = abstractJobTruckDriversServices.JobTruckDrivers_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobTruckDrivers_ByJobId")]
        public async Task<IHttpActionResult> JobTruckDrivers_ByJobId(PageParam pageParam,string search="", int JobId=0, int DriverId=0, int TruckId=0)
        {
            var quote = abstractJobTruckDriversServices.JobTruckDrivers_ByJobId(pageParam,search, JobId, DriverId, TruckId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("JobTruckDrivers_Delete")]
        public async Task<IHttpActionResult> JobTruckDrivers_Delete(int Id)
        {
            var quote = abstractJobTruckDriversServices.JobTruckDrivers_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        





    }
}
