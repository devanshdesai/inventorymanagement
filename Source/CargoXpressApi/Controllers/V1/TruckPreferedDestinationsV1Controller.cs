﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class TruckPreferedDestinationsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTruckPrefferedDestinationsServices abstractTruckPreferedDestinationsServices;

        #endregion

        #region Cnstr
        public TruckPreferedDestinationsV1Controller(AbstractTruckPrefferedDestinationsServices abstractTruckPreferedDestinationsServices)
        {
            this.abstractTruckPreferedDestinationsServices = abstractTruckPreferedDestinationsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckPreferedDestinations_Upsert")]
        public async Task<IHttpActionResult> TruckPreferedDestinations_Upsert(TruckPrefferedDestinations truckPreferedDestinations)
        {            
            var quote = abstractTruckPreferedDestinationsServices.TruckPrefferedDestinations_Upsert(truckPreferedDestinations);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckPreferedDestinations_ById")]
        public async Task<IHttpActionResult> TruckPreferedDestinations_ById(int Id)
        {
            var quote = abstractTruckPreferedDestinationsServices.TruckPrefferedDestinations_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckPreferedDestinations_All")]
        public async Task<IHttpActionResult> TruckPreferedDestinations_All(PageParam pageParam,int TruckId = 0)
        {
            var quote = abstractTruckPreferedDestinationsServices.TruckPrefferedDestinations_All(pageParam,TruckId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
