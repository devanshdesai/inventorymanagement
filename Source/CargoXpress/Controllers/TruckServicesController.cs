﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using GoogleMaps.LocationServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;
using System.Device.Location;

namespace CargoXpress.Controllers
{
    public class TruckServicesController : BaseController
    {
        #region Fields
        private readonly AbstractTruckServicesServices abstractTruckServicesServices;
        private readonly AbstractTruckTypesServices abstractTruckTypesServices;
        #endregion

        #region Ctor

        public TruckServicesController(AbstractTruckServicesServices abstractTruckServicesServices, AbstractTruckTypesServices abstractTruckTypesServices)
        {
            this.abstractTruckServicesServices = abstractTruckServicesServices;
            this.abstractTruckTypesServices = abstractTruckTypesServices;
        }
        #endregion
        
        #region Methods
        [ActionName(Actions.Index)]
        public ActionResult Index(int Id = 0)
        {
            ViewBag.TruckType = BindTruckTypeDropDown();
            ViewBag.Id = Id;
            return View();
        }



        public IList<SelectListItem> BindTruckTypeDropDown()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;


                var model = abstractTruckTypesServices.TruckTypes_All(pageParam, "");
                foreach (var master in model.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString() , Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int ParentId=0)
        {   
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractTruckServicesServices.TruckServices_All(pageParam,search, ParentId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }       

        [HttpPost]
        public JsonResult GetById(int Id)
        {
            var model = abstractTruckServicesServices.TruckServices_ById(Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }


       
        [HttpPost]
        public JsonResult Upsert(int Id , String Name, int ParentId)
        {
            AbstractTruckServices abstractTruckServices = new TruckServices();
            abstractTruckServices.Id = Id;
            abstractTruckServices.Name = Name;
            abstractTruckServices.ParentId = ParentId;

          
            var model = abstractTruckServicesServices.TruckServices_Upsert(abstractTruckServices);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

       

        #endregion
    }
}