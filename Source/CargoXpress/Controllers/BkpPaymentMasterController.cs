﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class BkpPaymentMasterController : BaseController
    {
        #region Fields
        private readonly AbstractPaymentMasterServices abstractPaymentMasterServices;
        private readonly AbstractUserTypeServices abstractUserTypeServices;
        private readonly AbstractJobServices abstractJobServices;
        #endregion 

        #region Ctor
        public BkpPaymentMasterController(AbstractPaymentMasterServices abstractPaymentMasterServices, AbstractUserTypeServices abstractUserTypeServices, AbstractJobServices abstractJobServices) 
        {
            this.abstractPaymentMasterServices = abstractPaymentMasterServices;
            this.abstractUserTypeServices = abstractUserTypeServices;
            this.abstractJobServices = abstractJobServices;
        }
        #endregion

        #region Methods

        [ActionName(Actions.Index)]
        public ActionResult Index(int JobId = 0)
        {
            ViewBag.PaymentFromUserType = BindDropDownsOne("paymentFromUserType");
            ViewBag.PaymentToUserType = BindDropDownsTwo("paymentToUserType");
            ViewBag.Type = BindDropDownsThree();
            ViewBag.JobId = JobId;
            ViewBag.JobNo = abstractJobServices.Job_ById(JobId).Item.JobNo;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,int JobId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractPaymentMaster abstractPaymentMaster = new PaymentMaster();



                var model = abstractPaymentMasterServices.PaymentMaster_All(pageParam, search, JobId);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> BindDropDownsOne(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                if (action == "paymentFromUserType")
                {
                    var model = abstractUserTypeServices.UserType_All(pageParam, "");
                    foreach (var master in model.Values)
                    {

                        if (master.Id == 1)
                        {
                            items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                        }
                        else if (master.Id == 3)
                        {
                            items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                        }
                    }
                }
                    return items;
                }
            catch (Exception ex)
            {
                return items;
            }
        }
        public IList<SelectListItem> BindDropDownsTwo(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;
                if (action == "paymentToUserType")
                    {
                        var model = abstractUserTypeServices.UserType_All(pageParam, "");
                        foreach (var master in model.Values)
                        {

                            if (master.Id == 1)
                            {
                                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                            }
                            else if (master.Id == 2)
                            {
                                items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                            }
                        }
                    }

                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }

        public IList<SelectListItem> BindDropDownsThree()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem { Text = "CR", Value = "CR" });
            items.Add(new SelectListItem { Text = "DR", Value = "DR" });


            return items;
          }
            





        public ActionResult PaymentMasterDetails(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            return View();
        }
        [HttpPost]
        public JsonResult GetById(int Id)
        {
            SuccessResult<AbstractPaymentMaster> successResult = abstractPaymentMasterServices.PaymentMaster_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Upsert(int Id,int JobId, int PaidByUserType, decimal Amount, string Type, int PaidToUserType, string Note, string PaymentDate)
        
        {
            AbstractPaymentMaster abstractPaymentMaster = new PaymentMaster();
            abstractPaymentMaster.Id = Id;
            abstractPaymentMaster.JobId = JobId;
            abstractPaymentMaster.PaidByUserType = PaidByUserType;
            abstractPaymentMaster.Amount = Amount;
            abstractPaymentMaster.Type = Type;
            abstractPaymentMaster.PaidToUserType = PaidToUserType;
            abstractPaymentMaster.Note = Note;
            abstractPaymentMaster.PaymentDate = PaymentDate;


            SuccessResult<AbstractPaymentMaster> result = abstractPaymentMasterServices.PaymentMaster_Upsert(abstractPaymentMaster);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeletePaymentMaster(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            abstractPaymentMasterServices.PaymentMaster_Delete(Id);
            return Json(200, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
