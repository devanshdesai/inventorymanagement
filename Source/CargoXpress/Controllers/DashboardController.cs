﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class DashboardController : BaseController
    {
        #region Fields
        
        private readonly AbstractJobBidsServices jobBidsServices;
        private readonly AbstractJobActivityServices jobActivityServices;
        //private readonly AbstractJobCommentsServices jobCommentsServices;
        //private readonly AbstractJobDocumentsServices jobDocumentsServices;
        //private readonly AbstractJobDriverLocationServices jobDriverLocationServices;
        //private readonly AbstractJobRatingsServices jobRatingsServices;
        private readonly AbstractJobServices jobServices;
        //private readonly AbstractJobStatusServices jobStatusServices;
        //private readonly AbstractMasterJobStatusServices masterJobStatusServices;
        #endregion

        #region Ctor
        //public DashboardController(
        //   AbstractJobCommentsServices jobCommentsServices,
        //    AbstractJobDocumentsServices jobDocumentsServices,
        //    AbstractJobDriverLocationServices jobDriverLocationServices,
        //    AbstractJobRatingsServices jobRatingsServices,
        //    AbstractJobServices jobServices,
        //    AbstractJobStatusServices jobStatusServices,
        //    AbstractMasterJobStatusServices masterJobStatusServices)
        public DashboardController(AbstractJobBidsServices jobBidsServices,
            AbstractJobServices jobServices,
            AbstractJobActivityServices jobActivityServices)
        {
            this.jobBidsServices = jobBidsServices;
            this.jobActivityServices = jobActivityServices;
            //this.jobCommentsServices = jobCommentsServices;
            //this.jobDocumentsServices = jobDocumentsServices;
            //this.jobDriverLocationServices = jobDriverLocationServices;
            //this.jobRatingsServices = jobRatingsServices;
            this.jobServices = jobServices;
            //this.jobStatusServices = jobStatusServices;
            //this.masterJobStatusServices = masterJobStatusServices;
        }
        #endregion
        #region Methods
        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            //int totalRecord = 0;

            //PageParam pageParam = new PageParam();
            //pageParam.Offset = 0;
            //pageParam.Limit = 1000;

            //string search = "";
            //var model = jobServices.Job_All(pageParam, search, null, 0, 0, 0, 0, 0, 0, DateTime.Now, DateTime.Now, DateTime.Now, "All");
            //totalRecord = (int)model.TotalRecords;

            //int[] NJSId = { 1, 2 };
            //ProjectSession.NewJobs = model.Values.Where(w => NJSId.Contains(w.JobStatusId)).Count();
            //int[] OJSId = {3, 4, 5, 6, 7, 8, 9};
            //ProjectSession.OngoingJobs = model.Values.Where(w => OJSId.Contains(w.JobStatusId)).Count();
            //ProjectSession.CompletedJobs = model.Values.Where(w => w.JobStatusId == 10).Count();
            //ProjectSession.CancelledJobs = model.Values.Where(w => w.JobStatusId == 11).Count();


            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindOngoingJobsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string JobStatus = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null, int PickUpShipperAddressId = 0, int DropOffShipperAddressId = 0, bool IsJobWithIssues = false, string PickupDate = null, int TruckServiceId = 0)
        {   
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractJob job = new Job();
                // var result = jobStatusServices.JobStatus_All(pageParam, search);
                // JobStatusId = result.Values.Where(w => w.Name == JobStatus).Select(s=>s.Id).FirstOrDefault();
                CarrierJobAssignedDate = DateTime.Now;
                CarrierJobStartDate = DateTime.Now;
                CarrierJobEndDate = DateTime.Now;

                var model = jobServices.Job_All(pageParam, search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, "OngoingJob", PickUpShipperAddressId, DropOffShipperAddressId, IsJobWithIssues, PickupDate, TruckServiceId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindNewJobsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string JobStatus = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null, int PickUpShipperAddressId = 0, int DropOffShipperAddressId = 0, bool IsJobWithIssues = false, string PickupDate = null, int TruckServiceId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractJob job = new Job();
                // var result = jobStatusServices.JobStatus_All(pageParam, search);
                //  JobStatusId = result.Values.Where(w => w.Name == JobStatus).Select(s => s.Id).FirstOrDefault();

                CarrierJobAssignedDate = DateTime.Now;
                CarrierJobStartDate = DateTime.Now;
                CarrierJobEndDate = DateTime.Now;

                var model = jobServices.Job_All(pageParam, search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, "OngoingJob", PickUpShipperAddressId, DropOffShipperAddressId, IsJobWithIssues, PickupDate, TruckServiceId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCompletedJobsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string JobStatus = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null, int PickUpShipperAddressId = 0, int DropOffShipperAddressId = 0, bool IsJobWithIssues = false, string PickupDate = null, int TruckServiceId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractJob job = new Job();
                // var result = jobStatusServices.JobStatus_All(pageParam, search);
                // JobStatusId = result.Values.Where(w => w.Name == JobStatus).Select(s => s.Id).FirstOrDefault();
                CarrierJobAssignedDate = DateTime.Now;
                CarrierJobStartDate = DateTime.Now;
                CarrierJobEndDate = DateTime.Now;

                var model = jobServices.Job_All(pageParam, search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, "CompletedJob", PickUpShipperAddressId, DropOffShipperAddressId, IsJobWithIssues, PickupDate, TruckServiceId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCancelledJobsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string JobStatus = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null, int PickUpShipperAddressId = 0, int DropOffShipperAddressId = 0, bool IsJobWithIssues = false, string PickupDate = null, int TruckServiceId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractJob job = new Job();
                //var result = jobStatusServices.JobStatus_All(pageParam, search);
                // JobStatusId = result.Values.Where(w => w.Name == JobStatus).Select(s => s.Id).FirstOrDefault();
                CarrierJobAssignedDate = DateTime.Now;
                CarrierJobStartDate = DateTime.Now;
                CarrierJobEndDate = DateTime.Now;

                var model = jobServices.Job_All(pageParam, search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, "CancelledJob", PickUpShipperAddressId, DropOffShipperAddressId, IsJobWithIssues, PickupDate, TruckServiceId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [ActionName(Actions.OngoingJobDetails)]
        public ActionResult OngoingJobDetails(int Id)
        {
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("OngoingJobDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }

        [ActionName(Actions.NewJobDetails)]
        public ActionResult NewJobDetails(int Id)
        {
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("NewJobDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobBidsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId)
        {   
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                int CarrierId = 0;

                //AbstractShippers abstractShippers = new Shippers();
                var model = jobBidsServices.JobBids_ByJobId(pageParam, JobId, CarrierId);

                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobActivityData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                int CarrierId = 0;

                //AbstractShippers abstractShippers = new Shippers();
                var model = jobActivityServices.JobActivity_ByJobId(pageParam, JobId, CarrierId);

                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.CompletedJobDetails)]
        public ActionResult CompletedJobDetails(int Id)
        {
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("CompletedJobDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }
        [ActionName(Actions.CancelledJobDetails)]
        public ActionResult CancelledJobDetails(int Id)
        {
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("CancelledJobDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }
        #endregion
    }
}