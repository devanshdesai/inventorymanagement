﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class DriversController : BaseController
    {
        #region Fields
        private readonly AbstractDriversService driversService;
        private readonly AbstractDriverDocumentsServices driverDocumentsServices;
        private readonly AbstractCountriesService countriesService;
        #endregion

        #region Ctor
        public DriversController(AbstractDriversService driversService,
            AbstractDriverDocumentsServices driverDocumentsServices,
            AbstractCountriesService countriesService)
        {
            this.driversService = driversService;
            this.driverDocumentsServices = driverDocumentsServices;
            this.countriesService = countriesService;
        }
        #endregion

        #region Methods        
        [ActionName(Actions.Index)]
        public ActionResult Index(int Id, string CarrierName)
        {
            ViewBag.CarrierId = Id;
            ProjectSession.CarrierName = CarrierName;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindDriversData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int CarrierId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = driversService.Drivers_All(pageParam, search, CarrierId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.DriverDetails)]
        public ActionResult DriverDetails(int Id)
        {
            
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 20;

            var result = driversService.Drivers_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("DriverDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindDriverDocumentsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int DriverID)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = driverDocumentsServices.DriverDocuments_All(pageParam, DriverID);

                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public JsonResult ActInact(int DriverId)
        {
            driversService.Drivers_ActInact(DriverId);
            return Json("Driver's status updated successfully", JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName(Actions.AddEditDriver)]
        public ActionResult AddEditDriver(int Id = 0, int CarrierId = 0)
        {
            ViewBag.Countries = BindCountryDropDown();
            if (Id == 0)
            {
                Drivers model = new Drivers();
                model.CarrierId = CarrierId;

                return View(model);
            }
            else
            {
                var result = driversService.Drivers_ById(Id);

                if (result != null && result.Code == 200 && result.Item != null)
                {
                    return View(result.Item);
                }
                else
                {
                    ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

                }
                return View();
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddEditDriver)]
        public ActionResult AddEditDriver(Drivers objmodel)
        {
            if (objmodel.Id == 0)
            {
                objmodel.Password = CommonHelper.RandomPassword();
            }
            var result = driversService.Drivers_Upsert(objmodel);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                return RedirectToAction("Index", new { Id = objmodel.CarrierId, CarrierName = ProjectSession.CarrierName });
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            ViewBag.Countries = BindCountryDropDown();
            return View(objmodel);
        }
        public IList<SelectListItem> BindCountryDropDown()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;


                var model = countriesService.Countries_All(pageParam, "");
                foreach (var master in model.Values)
                {
                    items.Add(new SelectListItem() { Text = master.CountryCode.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }
        #endregion
    }
}