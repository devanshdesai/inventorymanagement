﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class TransactionReportController : BaseController
    {
        #region Fields
        private readonly AbstractPaymentMasterServices abstractPaymentMasterServices;
        private readonly AbstractUserTypeServices abstractUserTypeServices;
        private readonly AbstractJobServices abstractJobServices;
        private readonly AbstractCarriersServices abstractCarriersServices;
        private readonly AbstractShippersServices abstractShippersServices;
        #endregion 

        #region Ctor
        public TransactionReportController(AbstractShippersServices abstractShippersServices, AbstractCarriersServices abstractCarriersServices,AbstractPaymentMasterServices abstractPaymentMasterServices, AbstractUserTypeServices abstractUserTypeServices, AbstractJobServices abstractJobServices) 
        {
            this.abstractPaymentMasterServices = abstractPaymentMasterServices;
            this.abstractUserTypeServices = abstractUserTypeServices;
            this.abstractJobServices = abstractJobServices;
            this.abstractShippersServices = abstractShippersServices;
            this.abstractCarriersServices = abstractCarriersServices;
        }
        #endregion

        #region Methods
        //int JobId = 0
        [ActionName(Actions.Index)]
        public ActionResult Index(int Id = 0)
        {
            ViewBag.MasterShipper = BindDropDowns("masterShipper");
            ViewBag.MasterCarrier = BindDropDowns("masterCarrier");
            ViewBag.Id = Id;
            //ViewBag.PaymentFromUserType = BindDropDownsOne("paymentFromUserType");
            //ViewBag.PaymentToUserType = BindDropDownsTwo("paymentToUserType");
            //ViewBag.Type = BindDropDownsThree();
            //ViewBag.IsRecieved = BindDropDownsFour();
            //ViewBag.JobId = JobId;
            //ViewBag.JobNo = abstractJobServices.Job_ById(JobId).Item.JobNo;
            return View();
        }
        public IList<SelectListItem> BindDropDowns(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;
                if (action == "masterShipper")
                {

                    var model = abstractShippersServices.Shippers_All(pageParam, "", "", "", 0, "", null);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.FirstName.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterCarrier")
                {
                    var model = abstractCarriersServices.Carriers_All(pageParam, "", "", "", 0, "", null);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.FullName.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
               

                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int ShipperId = 0, int CarrierId = 0, string FromDate = null, string ToDate = null)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractPaymentMaster abstractPaymentMaster = new PaymentMaster();
                
                //if ((ShipperId > 0) && (CarrierId == 0))
                //{
                //    abstractPaymentMaster.PaidByUserType = 3;
                //    abstractPaymentMaster.PaidToUserType = 1;
                //}
                //else if ((ShipperId == 0) && (CarrierId > 0))
                //{
                //    abstractPaymentMaster.PaidByUserType = 1;
                //    abstractPaymentMaster.PaidToUserType = 2;
                //}
                //else
                //{
                //    abstractPaymentMaster.PaidByUserType = 0;
                //    abstractPaymentMaster.PaidToUserType = 0;
                //}

                var model = abstractPaymentMasterServices.TransactionReport_ADVANCED(pageParam, search, ShipperId, CarrierId, FromDate, ToDate);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
       
        public ActionResult PaymentMasterDetails(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            return View();
        }
        [HttpPost]
        public JsonResult GetById(int Id)
        {
            SuccessResult<AbstractPaymentMaster> successResult = abstractPaymentMasterServices.PaymentMaster_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

      

      
        #endregion

    }
}
