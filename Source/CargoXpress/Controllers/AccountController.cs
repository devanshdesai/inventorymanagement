﻿using CargoXpress.Common;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class AccountController : Controller
    {
        #region Fields
        private readonly AbstractAdminServices adminService;
        #endregion

        #region Ctor
        public AccountController(AbstractAdminServices adminService)
        {
            this.adminService = adminService;
        }
        #endregion

        #region Methods
        [HttpGet]
        [ActionName(Actions.LogIn)]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.LogIn)]
        public ActionResult LogIn(Admin objmodel)
        {           

            
            SuccessResult<AbstractAdmin> result = adminService.Admin_Login(objmodel);
            if (result != null && result.Code == 200 && result.Item != null)
            {   
                Session.Clear();
                ProjectSession.AdminUserID = result.Item.Id;
                ProjectSession.UserName = result.Item.FirstName + " " + result.Item.LastName;
                ProjectSession.Email = result.Item.Email;


                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Values.Add("UserName", ProjectSession.UserName);
                cookie.Values.Add("Email", ProjectSession.Email);
                cookie.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookie);
                return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
            }
            return View();
        }

        [AllowAnonymous]
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if(Request.Cookies["UserLogin"] != null)
            {
                var c = new HttpCookie("UserLogin");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }

            return RedirectToAction(Actions.LogIn, CargoXpress.Pages.Controllers.Account);
        }
        #endregion
    }
}