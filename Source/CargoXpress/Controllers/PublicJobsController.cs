﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class PublicJobsController : Controller
    {
        #region Fields
        
        private readonly AbstractJobBidsServices jobBidsServices;
        private readonly AbstractJobActivityServices jobActivityServices;
        private readonly AbstractJobServices jobServices;
        private readonly AbstractJobDriverCommentsServices jobDriverCommentsServices;
        private readonly AbstractJobCommentsServices jobCommentsServices;
        private readonly AbstractJobDocumentsServices jobDocumentsServices;
        private readonly AbstractJobDriverLocationServices abstractJobDriverLocationServices;
        private readonly AbstractJobRatingsServices abstractJobRatingsServices;
        private readonly AbstractJobBillOfLoadingServices abstractJobBillOfLoadingServices;
        private readonly AbstractJobTruckDriversServices abstractJobTruckDriversServices;
        private readonly AbstractRouteMasterServices abstractRouteMasterServices;
        private readonly AbstractCountriesService abstractCountriesServices;
        private readonly AbstractTruckServicesServices abstractTruckServicesServices;
        private readonly AbstractCarriersServices abstractCarriersServices;
        private readonly AbstractShippersServices abstractShippersServices;
        private readonly AbstractTrucksService abstractTrucksService;
        private readonly AbstractDriversService abstractDriversService;

        #endregion

        #region Ctor

        public PublicJobsController(AbstractJobBidsServices jobBidsServices,
            AbstractJobServices jobServices,
            AbstractJobActivityServices jobActivityServices,
            AbstractJobDriverCommentsServices jobDriverCommentsServices,
            AbstractJobCommentsServices jobCommentsServices,
            AbstractJobDocumentsServices jobDocumentsServices,
            AbstractJobDriverLocationServices abstractJobDriverLocationServices,
            AbstractJobRatingsServices abstractJobRatingsServices,
            AbstractJobBillOfLoadingServices abstractJobBillOfLoadingServices,
            AbstractJobTruckDriversServices abstractJobTruckDriversServices,
            AbstractRouteMasterServices abstractRouteMasterServices,
            AbstractCountriesService abstractCountriesServices,
            AbstractTruckServicesServices abstractTruckServicesServices,
            AbstractCarriersServices abstractCarriersServices,
             AbstractShippersServices abstractShippersServices,
             AbstractTrucksService abstractTrucksService,
             AbstractDriversService abstractDriversService
            )
        {
            this.jobBidsServices = jobBidsServices;
            this.jobActivityServices = jobActivityServices;           
            this.jobServices = jobServices;
            this.jobDriverCommentsServices = jobDriverCommentsServices;
            this.jobCommentsServices = jobCommentsServices;
            this.jobDocumentsServices = jobDocumentsServices;
            this.abstractJobDriverLocationServices = abstractJobDriverLocationServices;
            this.abstractJobRatingsServices = abstractJobRatingsServices;
            this.abstractJobBillOfLoadingServices = abstractJobBillOfLoadingServices;
            this.abstractJobTruckDriversServices = abstractJobTruckDriversServices;
            this.abstractRouteMasterServices = abstractRouteMasterServices;
            this.abstractCountriesServices = abstractCountriesServices;
            this.abstractTruckServicesServices = abstractTruckServicesServices;
            this.abstractTrucksService = abstractTrucksService;
            this.abstractShippersServices = abstractShippersServices;
            this.abstractCarriersServices = abstractCarriersServices;
            this.abstractDriversService = abstractDriversService;
        }
        #endregion

        #region Methods
       
        [HttpPost]
        public JsonResult BindJsonDropdown(string action="",int CarrierId = 0)
        {
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;
                if (action == "masterDriver")
                {
                    var model = abstractDriversService.Drivers_All(pageParam, "", CarrierId);
                    return Json(model.Values, JsonRequestBehavior.AllowGet);
                }
                else if (action == "masterTruck")
                {
                    var model = abstractTrucksService.Trucks_All(pageParam, "", "", "", 0, 0, CarrierId);
                    return Json(model.Values, JsonRequestBehavior.AllowGet);
                }
                return Json("400", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public IList<SelectListItem> BindDropDowns(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;
                if (action == "masterCountries")
                {

                    var model = abstractCountriesServices.Countries_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterTruckServices")
                {

                    var model = abstractTruckServicesServices.TruckServices_All(pageParam, "", 0);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterShipper")
                {
                   
                    var model = abstractShippersServices.Shippers_All(pageParam,"","","",0,"",null);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.FirstName.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterCarrier")
                {
                    var model = abstractCarriersServices.Carriers_All(pageParam, "", "", "", 0, "", null);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.FullName.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterDriver")
                {
                    var model = abstractDriversService.Drivers_All(pageParam, "", 0);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.FullName.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterTruck")
                {
                    var model = abstractTrucksService.Trucks_All(pageParam, "", "", "", 0, 0, 0);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.TruckTypeName.ToString() + master.PlateNumber.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }

                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindOngoingJobsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string JobStatus = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null, int PickUpShipperAddressId=0, int DropOffShipperAddressId=0, bool IsJobWithIssues = false, string PickupDate=null, int TruckServiceId=0)
        {   
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractJob job = new Job();
                // var result = jobStatusServices.JobStatus_All(pageParam, search);
                // JobStatusId = result.Values.Where(w => w.Name == JobStatus).Select(s=>s.Id).FirstOrDefault();
               

                var model = jobServices.Job_All(pageParam, search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, "OngoingJob", PickUpShipperAddressId, DropOffShipperAddressId, IsJobWithIssues, PickupDate, TruckServiceId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }


        [ActionName(Actions.Index)]
        public ActionResult Index(int Id)
        {
            ViewBag.JobsId = Id;
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("Index", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobActivityData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                int CarrierId = 0;

                //AbstractShippers abstractShippers = new Shippers();
                var model = jobActivityServices.JobActivity_ByJobId(pageParam, JobId, CarrierId);

                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.CompletedJobDetails)]
        public ActionResult CompletedJobDetails(int Id)
        {
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                PageParam page = new PageParam();
                var shipperratingResult = abstractJobRatingsServices.JobRatings_ByJobId(page, Id, 0, 0, 2);
                if(shipperratingResult.Values.Count > 0)
                {
                    result.Item.RatingToShipper = ConvertTo.Decimal(shipperratingResult.Values[0].RatingToShipper);
                    result.Item.CommentToShipper = shipperratingResult.Values[0].CommentToShipper;
                    result.Item.RatingToShipperDate = shipperratingResult.Values[0].RatingToShipperDateStr;
                }
                var carrierratingResult = abstractJobRatingsServices.JobRatings_ByJobId(page, Id, 0, 0, 3);
                if (shipperratingResult.Values.Count > 0)
                {
                    result.Item.RatingToCarrier = ConvertTo.Decimal(carrierratingResult.Values[0].RatingToShipper);
                    result.Item.CommentToCarrier = carrierratingResult.Values[0].CommentToShipper;
                    result.Item.RatingToCarrierDate= carrierratingResult.Values[0].RatingToShipperDateStr;
                }
                return View("CompletedJobDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }

        public ActionResult TruckLocation(int JobId,int RouteMasterId)
        {
            PageParam pageParam = new PageParam();
            //var result = abstractJobDriverLocationServices.JobDriverLocation_ByJobId(pageParam, JobId, 0);
            var result2 = jobServices.Job_ById(JobId);
            //if (result.Values.Count > 0)
            //{
            //    ViewBag.Latitude = result.Values[result.Values.Count-1].Latitude;
            //    ViewBag.Longitude = result.Values[result.Values.Count - 1].Longitude;
            //    ViewBag.CapturedDate = result.Values[result.Values.Count - 1].CapturedDate;
            //}
            //else
            //{
            //    ViewBag.Latitude = 0.0;
            //    ViewBag.Longitude = 0.0;
            //    ViewBag.CapturedDate = "";
            //}
            ViewBag.JobId = JobId;
            ViewBag.RouteMasterId = RouteMasterId;
            if (result2 != null && result2.Code == 200 && result2.Item != null)
            {
                ViewBag.JobNo = result2.Item.JobNo;
            }
            else
            {
                ViewBag.JobNo = "";
            }

            return View();
        }

        public JsonResult TruckLocationCo(int JobId, int RouteMasterId)
        {
            var rtMaster = abstractRouteMasterServices.RouteMaster_ById(RouteMasterId);
            int driverId = 0;
            if (rtMaster.Item != null)
            {
                var jobTruckDriver = abstractJobTruckDriversServices.JobTruckDrivers_ById(rtMaster.Item.JobTruckDriverId);
                if (jobTruckDriver.Item != null)
                {
                    driverId = jobTruckDriver.Item.DriverId;
                }
            }

            PageParam pageParam = new PageParam();
            var result = abstractJobDriverLocationServices.JobDriverLocation_ByJobId(pageParam, JobId, driverId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobTruckDriversData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                //AbstractShippers abstractShippers = new Shippers();
                var model = abstractJobTruckDriversServices.JobTruckDrivers_ByJobId(pageParam, search, JobId);

                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}