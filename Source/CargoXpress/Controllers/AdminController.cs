﻿using CargoXpress.Common;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
   
    public class AdminController : BaseController
    {
        #region Fields
        private readonly AbstractAdminServices adminService;
        #endregion

        #region Ctor
        public AdminController(AbstractAdminServices adminService)
        {
            this.adminService = adminService;
        }
        #endregion

        #region Methods
        [HttpGet]
        [ActionName(Actions.ChangePassword)]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.ChangePassword)]
        public ActionResult ChangePassword(Admin objmodel)
        {

            objmodel.Id = ProjectSession.AdminUserID;
            var result = adminService.Admin_ChangePassword(objmodel);
            if (result == true)
            {                
                return RedirectToAction(Actions.LogIn, Pages.Controllers.Account);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result);
            }
            return View();
        }      
       
        #endregion
    }
}