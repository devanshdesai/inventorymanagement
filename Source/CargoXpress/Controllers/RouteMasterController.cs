﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using GoogleMaps.LocationServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;
using System.Device.Location;

namespace CargoXpress.Controllers
{
    public class RouteMasterController : BaseController
    {
        #region Fields
        private readonly AbstractJobServices jobServices;
        private readonly AbstractRouteMasterServices abstractRouteMasterServices;
        private readonly AbstractRouteChildServices abstractRouteChildServices;
        public readonly AbstractJobTruckDriversServices abstractJobTruckDriversServices;
        private readonly AbstractJobDriverLocationServices abstractJobDriverLocationServices;
        #endregion

        #region Ctor

        public RouteMasterController(AbstractJobServices jobServices, AbstractRouteMasterServices abstractRouteMasterServices, 
            AbstractRouteChildServices abstractRouteChildServices, 
            AbstractJobTruckDriversServices abstractJobTruckDriversServices,
            AbstractJobDriverLocationServices abstractJobDriverLocationServices)
        {
            this.jobServices = jobServices;
            this.abstractRouteMasterServices = abstractRouteMasterServices;
            this.abstractRouteChildServices = abstractRouteChildServices;
            this.abstractJobTruckDriversServices = abstractJobTruckDriversServices;
            this.abstractJobDriverLocationServices = abstractJobDriverLocationServices;
        }
        #endregion
        
        #region Methods
        [ActionName(Actions.Index)]
        public ActionResult Index(int JobId)
        {
            var item = jobServices.Job_ById(JobId);
            ViewBag.JobTruckDrivers = BindJobTruckDriversDropDown(JobId);
            if (item.Item != null)
            {
                ViewBag.JobNo = item.Item.JobNo;
                ViewBag.JobId = item.Item.Id;
            }
            else
            {

            }

            return View();
        }

        public IList<SelectListItem> BindJobTruckDriversDropDown(int JobId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;


                var model = abstractJobTruckDriversServices.JobTruckDrivers_ByJobId(pageParam, "" , JobId);
                foreach (var master in model.Values)
                {
                    items.Add(new SelectListItem() { Text = master.DriverFirstName.ToString() + '/' + master.TruckName.ToString() + '-' + master.TruckPlateNumber.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId = 0)
        {   
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractRouteMasterServices.RouteMaster_ByJobId(pageParam,search,JobId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindDatas([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int RouteMasterId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractRouteChildServices.RouteChild_ByRouteMasterId(pageParam, search, RouteMasterId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetById(int Id)
        {
            var model = abstractRouteMasterServices.RouteMaster_ById(Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetByIds(int Id)
        {
            var model = abstractRouteChildServices.RouteChild_ById(Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(int Id)
        {
            var model = abstractRouteMasterServices.RouteMaster_Delete(Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Deletes(int Id)
        {
            var model = abstractRouteChildServices.RouteChild_Delete(Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Upsert(int Id=0, int JobId=0, int JobTruckDriverId=0, string PickUpLocationName="",string PickUpLat = "", string PickUpLong = "", string DropOffLocationName = "", string DropOffLat = "", string DropOffLong = "")
        {
            AbstractRouteMaster abstractRouteMaster = new RouteMaster();
            abstractRouteMaster.Id = Id;
            abstractRouteMaster.JobId = JobId;
            abstractRouteMaster.JobTruckDriverId = JobTruckDriverId;

            abstractRouteMaster.PickUpLocationName = PickUpLocationName;
            DataTable dt = GetLatLong(PickUpLocationName);
            if (dt.Rows.Count > 0)
            {
                abstractRouteMaster.PickUpLat = Convert.ToString(dt.Rows[0]["Latitude"]);
                abstractRouteMaster.PickUpLong = Convert.ToString(dt.Rows[0]["Longitude"]);
            }

            abstractRouteMaster.DropOffLocationName = DropOffLocationName;
            dt = GetLatLong(DropOffLocationName);
            if (dt.Rows.Count > 0)
            {
                abstractRouteMaster.DropOffLat = Convert.ToString(dt.Rows[0]["Latitude"]);
                abstractRouteMaster.DropOffLong = Convert.ToString(dt.Rows[0]["Longitude"]);
            }

            var model = abstractRouteMasterServices.RouteMaster_Upsert(abstractRouteMaster);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Upserts(int Id = 0, int RouteMasterId = 0, string LocationName = "")
        {

            PageParam pageParam = new PageParam();
            var allRouteChild = abstractRouteChildServices.RouteChild_ByRouteMasterId(pageParam, "", RouteMasterId);


            var routeMasterDetails = abstractRouteMasterServices.RouteMaster_ById(RouteMasterId);

            AbstractRouteChild abstractRouteMaster = new RouteChild();
            abstractRouteMaster.Id = Id;
            abstractRouteMaster.RouteMasterId = RouteMasterId;

            DataTable dt = GetLatLong(LocationName);
          
            abstractRouteMaster.LocationName = LocationName;
            if(dt.Rows.Count > 0)
            {
                abstractRouteMaster.Lat = Convert.ToString(dt.Rows[0]["Latitude"]);
                abstractRouteMaster.Long = Convert.ToString(dt.Rows[0]["Longitude"]);
                double distance = 0;

                for (int i = 0; i < allRouteChild.Values.Count; i++)
                {
                    distance = distance + Convert.ToDouble(allRouteChild.Values[i].Distance);
                }


                if (routeMasterDetails.Item != null)
                {
                    var sCoord = new GeoCoordinate(Convert.ToDouble(routeMasterDetails.Item.PickUpLat), Convert.ToDouble(routeMasterDetails.Item.PickUpLong));
                    var eCoord = new GeoCoordinate(Convert.ToDouble(dt.Rows[0]["Latitude"]), Convert.ToDouble(dt.Rows[0]["Longitude"]));
                    if (allRouteChild.Values.Count > 0)
                    {
                        var lastCord = new GeoCoordinate(Convert.ToDouble(allRouteChild.Values[allRouteChild.Values.Count - 1].Lat), Convert.ToDouble(allRouteChild.Values[allRouteChild.Values.Count - 1].Long));
                        distance = distance + Convert.ToDouble((lastCord.GetDistanceTo(eCoord) / 1000).ToString("00.00"));
                    }
                    else
                    {
                        distance = Convert.ToDouble((sCoord.GetDistanceTo(eCoord) / 1000).ToString("00.00"));
                    }

                    //distance = Convert.ToDouble((sCoord.GetDistanceTo(eCoord) / 1000).ToString("00.00"));
                }
                
                abstractRouteMaster.Distance = Convert.ToString(distance);
            }
            else
            {
                abstractRouteMaster.Distance = "0";
            }

            var model = abstractRouteChildServices.RouteChild_Upsert(abstractRouteMaster);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private DataTable GetLatLong(string address)
        {
            //string url = "https://maps.google.com/maps/api/geocode/xml?address=" + address + "&key=AIzaSyABofRnRkpT_4m344ZOsyRnT90uSGF5H6A";
            //string url = "https://maps.google.com/maps/api/geocode/xml?address=" + address + "&key=AIzaSyAJyahQ-oUd7nU22mj6EUl-aRoMeRLevfA";
            string url = "https://maps.google.com/maps/api/geocode/xml?address=" + address + "&key=AIzaSyABofRnRkpT_4m344ZOsyRnT90uSGF5H6A";
            WebRequest request = WebRequest.Create(url);

            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                DataTable dtCoordinates = new DataTable();
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    DataSet dsResult = new DataSet();
                    dsResult.ReadXml(reader);
                    dtCoordinates.Columns.AddRange(new DataColumn[4] { new DataColumn("Id", typeof(int)),
                    new DataColumn("Address", typeof(string)),
                    new DataColumn("Latitude",typeof(string)),
                    new DataColumn("Longitude",typeof(string)) });
                    foreach (DataRow row in dsResult.Tables["result"].Rows)
                    {
                        string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
                        DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
                        dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);
                    }
                }
                return dtCoordinates;
            }
        }

        public JsonResult TruckLocationCo(int JobId, int RouteMasterId)
        {
            var rtMaster = abstractRouteMasterServices.RouteMaster_ById(RouteMasterId);
            int driverId = 0;
            if(rtMaster.Item != null)
            {
                var jobTruckDriver = abstractJobTruckDriversServices.JobTruckDrivers_ById(rtMaster.Item.JobTruckDriverId);
                if(jobTruckDriver.Item != null)
                {
                    driverId = jobTruckDriver.Item.DriverId;
                }
            }

            PageParam pageParam = new PageParam();
            var result = abstractJobDriverLocationServices.JobDriverLocation_ByJobId(pageParam, JobId, driverId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}