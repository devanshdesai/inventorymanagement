﻿namespace CargoXpress.Pages
{
    public class Actions
    {
        #region Authentication
        public const string SignIn = "SignIn";
        public const string RecoverPassword = "RecoverPassword";
        
        #endregion

        #region Home
        public const string Index = "Index";
        public const string SampleAdd = "SampleAdd";
        public const string BindLocationData = "BindLocationData";
        #endregion

        #region Account
        public const string LogIn = "LogIn";
        public const string Logout = "Logout";        
        #endregion

        #region Admin
        public const string ChangePassword = "ChangePassword";
        #endregion

        #region Shippers
        public const string BindShippersData = "BindShippersData";
        public const string ShipperDetails = "ShipperDetails";
        public const string BindShipperAddressesData = "BindShipperAddressesData";
        public const string BindShipperDocumentsData = "BindShipperDocumentsData";
        public const string ActInact = "ActInact";
        public const string AddEditShipper = "AddEditShipper";
        #endregion

        #region Carriers
        public const string BindCarriersData = "BindCarriersData";
        public const string CarrierDetails = "CarrierDetails";
        public const string BindCarrierDocumentsData = "BindCarrierDocumentsData";
        public const string BindCarrierTrucksData = "BindCarrierTrucksData";
        public const string BindCarrierPreferredDestData = "BindCarrierPreferredDestData";
        public const string AddEditCarrier = "AddEditCarrier";
        public const string ApprovedCarrier = "ApprovedCarrier";
        #endregion

        #region Drivers
        public const string BindDriversData = "BindDriversData";
        public const string DriverDetails = "DriverDetails";
        public const string BindDriverDocumentsData = "BindDriverDocumentsData";
        public const string AddEditDriver = "AddEditDriver";
        #endregion

        #region Trucks
        public const string BindTrucksData = "BindTrucksData";
        public const string TruckDetails = "TruckDetails";
        public const string BindTruckDocumentsData = "BindTruckDocumentsData";
        public const string BindTruckPreferredDestData = "BindTruckPreferredDestData";
        public const string AddEditTruck = "AddEditTruck";
        #endregion

        #region Jobs
        public const string BindOngoingJobsData = "BindOngoingJobsData";
        public const string BindNewJobsData = "BindNewJobsData";
        public const string BindCompletedJobsData = "BindCompletedJobsData";
        public const string BindCancelledJobsData = "BindCancelledJobsData";
        public const string OngoingJobDetails = "OngoingJobDetails";
        public const string NewJobDetails = "NewJobDetails";
        public const string CompletedJobDetails = "CompletedJobDetails";
        public const string CancelledJobDetails = "CancelledJobDetails";
        public const string BindJobBidsData = "BindJobBidsData";
        public const string BindBillOfLoading = "BindBillOfLoading";
        public const string BindJobDocumentsData = "BindJobDocumentsData";
        public const string BindJobActivityData = "BindJobActivityData";
        public const string BindJobDriverCommentsData = "BindJobDriverCommentsData";
        public const string BindJobCommentsData = "BindJobCommentsData";
        public const string GetJobCommentById = "GetJobCommentById";
        public const string AddEditJobComment = "AddEditJobComment";
        public const string DeleteJobComment = "DeleteJobComment";
        public const string BindJobTruckDriversData = "BindJobTruckDriversData";
        #endregion

        #region Common
        public const string Delete = "Delete";
        public const string GetById = "GetById";
        public const string Upsert = "Upsert";
        public const string BindData = "BindData";
        public const string BindDatas = "BindDatas";
        public const string Upserts = "Upserts";
        public const string Deletes = "Deletes";
        public const string GetByIds = "GetByIds";
        #endregion

        #region MasterPricingPlans
        //public const string BindData = "BindData";
        public const string MasterPricingPlansDetails = "MasterPricingPlansDetails";
        ////public const string BindShipperAddressesData = "BindShipperAddressesData";
        ////public const string BindShipperDocumentsData = "BindShipperDocumentsDatak";
        //public const string ActInact = "ActInact";
        //public const string AddEditMasterPricingPlans = "AddEditMasterPricingPlans";
        #endregion

        #region SupportDetails
        public const string SupportDetails = "SupportDetails";
        public const string BindSupportReply = "BindSupportReply";
        #endregion

        #region JobDetails
        public const string JobDetails = "JobDetails";
        #endregion

        #region PaymentMaster
        public const string DeletePaymentMaster = "DeletePaymentMaster";
        public const string BindPaymentMaster = "BindPaymentMaster";
        #endregion
    }
}